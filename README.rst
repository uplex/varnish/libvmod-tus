========
vmod-tus
========

This version is for Varnish-Cache 7 and later

DESCRIPTION
===========

.. _tus: https://tus.io/protocols/resumable-upload.html
.. _vmod_blobdigest: https://code.uplex.de/uplex-varnish/libvmod-blobdigest

This vmod implements a `tus`_ proxy which collects uploads on the
varnish server to send them to a backend in one go for storage. It
does not implement any permanent storage itself.

Besides the basic resumable uploads specified as the `tus`_ core
protocol, all currently defined extensions are supported, in
particular concatenation uploads.

See vmod documentation/man page for details on how to use it.

INSTALLATION
============

The source tree is based on autotools to configure the building, and
does also have the necessary bits in place to do functional unit tests
using the ``varnishtest`` tool.

Besides the Varnish header files (as located via ``pkg-config``), this
vmod also requires

* The ``VARNISHSRC`` Variable to point to the varnish-cache sources
  used for building the installed varnish version.

* `vmod_blobdigest`_ to be installed before building this vmod if
  hashes are to be used.

This vmod will need to be rebuild specifically for each version of
varnish and `vmod_blobdigest`_ as it requires internal interfaces.

Usage::

 export VARNISHSRC=/path/to/your/varnish/sources
 ./bootstrap

If you have installed Varnish to a non-standard directory, call
``autogen.sh`` and ``bootstrap`` with ``PKG_CONFIG_PATH`` pointing to
the appropriate path. For instance, when varnishd configure was called
with ``--prefix=$PREFIX``, use

::

 export PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
 export ACLOCAL_PATH=${PREFIX}/share/aclocal

The module will inherit its prefix from Varnish, unless you specify a
different ``--prefix`` when running the ``bootstrap`` or ``configure``
scripts for this module.

Make targets:

* make - builds the vmod.
* make install - installs your vmod.
* make check - runs the unit tests in ``src/vtc/*.vtc``.
* make distcheck - run check and prepare a tarball of the vmod.

If you build a dist tarball, you don't need any of the autotools or
pkg-config. You can build the module simply by running::

 ./configure
 make

Installation directories
------------------------

By default, the vmod ``configure`` script installs the built vmod in the
directory relevant to the prefix. The vmod installation directory can be
overridden by passing the ``vmoddir`` variable to ``make install``.

COPYRIGHT
=========

::

  Copyright 2020 UPLEX Nils Goroll Systemoptimierung
  All rights reserved

  This document is licensed under the same conditions as the libvmod-tus
  project. See LICENSE for details.

  Author: Nils Goroll <nils.goroll@uplex.de>
