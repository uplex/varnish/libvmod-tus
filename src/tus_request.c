/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <inttypes.h>
#include <unistd.h>
#include <time.h>

#include <cache/cache.h>
#include <vsb.h>

#include "tus_file.h"
#include "tus_hdr.h"
#include "tus_response.h"
#include "tus_request.h"
#include "tus_stv.h"
#include "tus_hex.h"

/* ------------------------------------------------------------
 * from https://github.com/varnishcache/varnish-cache/pull/3407
 */

#define vct_ishdrval(x) (((x) >= 0x20 && (x) != 0x7f) || (x) == 0x09)

static inline VCL_BOOL
validhdr(const char *p)
{
	AN(p);
	for(;*p != '\0'; p++)
		if (! vct_ishdrval(*p))
			return (0);
	return (1);
}


/* ------------------------------------------------------------
 */
/*lint -esym(749, met_e::_MET_MAX) */
enum met_e {
	_INVALID = 0,
#define MET(x) x,
#include "tbl_method.h"
	_MET_MAX
};

static enum met_e
parse_met(const char *s)
{
#define MET(x) if (strcmp(s, #x) == 0) return (x);
#include "tbl_method.h"
	return (_INVALID);
}

// everything for a tus upload to the backend is complete
static VCL_BOOL
tus_request_complete(VRT_CTX, const struct VPFX(tus_server) *srv,
    struct tus_concat *c, const struct tus_file_disk *fdisk)
{
	VCL_BLOB b = NULL;
	struct vsb vsb[1];
	const char *p;

	tus_body_assign(ctx->req, c);
	http_ForceField(ctx->http_req, HTTP_HDR_METHOD, "PUT");
	if (fdisk->type == TUS_SINGLE)
		b = tus_concat_hash(ctx, srv, c);
	if (b != NULL) {
		p = strrchr(fdisk->upload_path, '/');
		AN(p);
		WS_VSB_new(vsb, ctx->ws);
		(void)VSB_bcat(vsb, fdisk->upload_path,
			       (p - fdisk->upload_path) + 1);
		tus_vsbhex(vsb, b);
		http_SetH(ctx->http_req, HTTP_HDR_URL,
		    WS_VSB_finish(vsb, ctx->ws, NULL));
	} else {
		http_SetH(ctx->http_req, HTTP_HDR_URL, fdisk->upload_path);
	}
	http_Unset(ctx->http_req, H_Content_Length);
	http_PrintfHeader(ctx->http_req, "Content-Length: %zu",
	    fdisk->upload_length);

	// https://github.com/tus/tus-resumable-upload-protocol/pull/160
	http_Unset(ctx->http_req, H_Content_Type);
	if (tus_file_meta(ctx, fdisk, "filetype", &b)) {
		WS_VSB_new(vsb, ctx->ws);
		(void)VSB_bcat(vsb, b->blob, (ssize_t)b->len);
		(void)VSB_putc(vsb, '\0');
		p = WS_VSB_finish(vsb, ctx->ws, NULL);
		if (validhdr(p))
			http_ForceHeader(ctx->http_req, H_Content_Type, p);
	}
	// move to table?
	http_Unset(ctx->http_req, hdr_resum);
	http_Unset(ctx->http_req, hdr_vers);
	http_Unset(ctx->http_req, hdr_ext);
	http_Unset(ctx->http_req, hdr_chkalg);
	http_Unset(ctx->http_req, hdr_chksum);
	http_Unset(ctx->http_req, hdr_max);
	http_Unset(ctx->http_req, hdr_defer);
	http_Unset(ctx->http_req, hdr_meta);
	http_Unset(ctx->http_req, hdr_exp);
	http_Unset(ctx->http_req, hdr_loc);
	http_Unset(ctx->http_req, hdr_method);
	http_Unset(ctx->http_req, hdr_off);
	http_Unset(ctx->http_req, hdr_len);

	return (1);
}

struct test_meta {
	struct test_meta *prev;
	const char *k;
	size_t l;
};

static VCL_BOOL
tus_meta_key_unique(struct test_meta *this, size_t l)
{
	struct test_meta *prev = this;

	if (l == 0)
		return (0);
	if (this->l == 0)
		this->l = l;

	while ((prev = prev->prev) != NULL) {
		if (l != prev->l || strncmp(this->k, prev->k, l) != 0)
			continue;
		return (0);
	}

	return (1);
}

// from vmod_blob base64.c
#define ILL ((int8_t) 127)
#define PAD ((int8_t) 126)
static const int8_t i64[256] = {
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL,  62, ILL, ILL, ILL,  63, /* +, -    */
	 52,  53,  54,  55,  56,  57,  58,  59, /* 0 - 7   */
	 60,  61, ILL, ILL, ILL, PAD, ILL, ILL, /* 8, 9, = */
	ILL,   0,   1,   2,   3,   4,   5,   6, /* A - G   */
	  7,   8,   9,  10,  11,  12,  13,  14, /* H - O   */
	 15,  16,  17,  18,  19,  20,  21,  22, /* P - W   */
	 23,  24,  25, ILL, ILL, ILL, ILL, ILL, /* X, Y, Z */
	ILL,  26,  27,  28,  29,  30,  31,  32, /* a - g   */
	 33,  34,  35,  36,  37,  38,  39,  40, /* h - o   */
	 41,  42,  43,  44,  45,  46,  47,  48, /* p - w   */
	 49,  50,  51, ILL, ILL, ILL, ILL, ILL, /* x, y, z */
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL
};

static VCL_BOOL
tus_metadata_validate(const char *s, struct test_meta *prev)
{
	struct test_meta this = {prev, s, 0};
	size_t sz;

	// key
	while (1) {
		if (*s != ',' && *s != ' ' && *s != '\0') {
			s++;
			continue;
		}

		assert(s >= this.k);
		sz = (size_t)(s - this.k);
		if (tus_meta_key_unique(&this, sz) == 0)
			return (0);

		if (*s == '\0')
			return (1);

		if (*s == ',' || (*s == ' ' && s[1] == ','))
			return (tus_metadata_validate(s + 1, &this));

		if (*s == ' ') {
			s++;
			break;
		}
	}
	while (*s != '\0') {
		if (*s == ',')
			return (tus_metadata_validate(s + 1, &this));
		if (i64[(uint8_t)*s] == ILL)
			return (0);
		s++;
	}
	return (1);
}


VCL_BOOL
tus_request(VRT_CTX, struct VPFX(tus_server) *tussrv,
    struct tus_response *r, const char *url, const char *id)
{
	struct tus_file_disk *fdisk = NULL;
	const char *p, *metadata = NULL, *concat = NULL;
	struct concat_embryo embryo;
	char *q;
	enum met_e m;
	int ct_ok, lock;
	uintmax_t cl, off = UINTMAX_MAX, len = UINTMAX_MAX;
	enum tus_f_type type = TUS_SINGLE;
	struct tus_concat *c;
	VCL_BLOB hash;

	AZ(r->s.status);
	AZ(r->s.reason);
	AZ(r->fcore);

	if (http_GetHdr(ctx->http_req, hdr_method, &p)) {
		http_ForceField(ctx->http_req, HTTP_HDR_METHOD, p);
		http_Unset(ctx->http_req, hdr_method);
	}

	m = parse_met(http_GetMethod(ctx->http_req));

	(void) http_GetHdr(ctx->http_req, hdr_origin, &r->origin);

	if (m == _INVALID) {
		r->s.status = 405;
		return (0);
	}
	if (m == OPTIONS) {
		r->s.status = 204 + (s_OPTIONS * s_MULT);
		return (0);
	}

	if (m != GET &&
	    (! http_GetHdr(ctx->http_req, hdr_resum, &p) ||
	     strcmp(p, "1.0.0") != 0)) {
		r->s.status = 412;
		return (0);
	}

	if (http_GetHdr(ctx->http_req, hdr_concat, &concat)) {
		if (strcmp(concat, "partial") == 0)
			type = TUS_PARTIAL;
		else if (TOK(concat, "final;"))
			type = TUS_FINAL;
		else {
			r->s.status = 400;
			return (0);
		}
	}

	if (m == POST && type == TUS_FINAL) {
		if (tus_file_final_concat(tussrv, &embryo, concat) == NULL) {
			r->s.status = 400;
			return (0);
		}
		hash = tus_concat_hash(ctx, embryo.srv, embryo.concat);
		if (hash != NULL) {
			id = tus_hex(ctx, hash);
			if (id == NULL) {
				r->s.status = 503;
				return (0);
			}
		}
	}

	if (http_GetHdr(ctx->http_req, hdr_meta, &metadata) &&
	    tus_metadata_validate(metadata, NULL) == 0) {
		VSLb(ctx->vsl, SLT_Error, "%s: bad metadata format",
		    tus_server_name(tussrv));
		r->s.status = 400;
		return (0);
	}

	tus_server_lock(tussrv);

	if (m == POST) {
		r->fcore = tus_file_new(ctx, tussrv, type, url, id, metadata,
		    &r->s.status, &r->s.reason);
	} else {
		r->fcore = tus_file_lookup(tussrv, url);
	}

	if (r->fcore != NULL) {
		r->schemeauth = WS_Copy(ctx->ws,
		    tus_server_schemeauth(tussrv), -1);
	}

	lock = tus_file_trylock(&r->fcore);
	tus_server_unlock(tussrv);

	if (lock == EBUSY) {
		AZ(r->fcore);
		r->s.status = 423;
		return (0);
	}

	if (r->fcore != NULL) {
		assert(lock == 0);
		fdisk = r->fcore->disk;
		AN(fdisk);
		type = fdisk->type;
	} else {
		assert(lock == EINVAL);
	}

	// from here on, we hold a lock on r->fcore (if != NULL), which we either
	// need to unlock or pass to the response, which unlocks via
	// tus_task_free()

	if (m == POST && type == TUS_FINAL) {
		if (r->fcore == NULL) {
			tus_file_final_abort(&embryo);
		} else {
			tus_file_final_birth(&r->fcore, &embryo);
			r->s.status = r->fcore ? 201 : 500;
		}
	}

	if (m == DELETE) {
		if (r->fcore == NULL) {
			r->s.status = 404;
			return (0);
		}

		tus_file_del(&r->fcore);
		AZ(r->fcore);
		r->s.status = 204;
		return (0);
	}

	if (m == GET) {
		if (r->fcore == NULL) {
			r->s.status = 404;
			return (0);
		}
		AN(r->fcore);
		AN(fdisk);
		if (fdisk->location_length > 0) {
			r->s.status = 301;	/* done file */
		} else {
			tus_file_unlock(&r->fcore);
			AZ(r->fcore);
			r->s.status = 400;
		}
		return (0);
	}

	if (m == HEAD) {
		r->s.status = r->fcore ? 200 : 404;
		return (0);
	}

	if (r->fcore == NULL) {
		if (m == POST) {
			// tus_file_new sets error
			AN(r->s.status);
			AN(r->s.reason);
		} else {
			r->s.status = 404;
		}
		return (0);
	}

	AN(r->fcore);
	AN(fdisk);

	ct_ok = http_GetHdr(ctx->http_req, H_Content_Type, &p) &&
	    (strcmp(p, "application/offset+octet-stream") == 0);

	if (http_GetHdr(ctx->http_req, hdr_off, &p)) {
		off = strtoumax(p, &q, 10);
		if (q == NULL || *q != '\0')
			off = UINTMAX_MAX;
	}

	if (http_GetHdr(ctx->http_req, hdr_len, &p)) {
		if (type == TUS_FINAL) {
			r->s.status = 400;
			return (0);
		}

		len = strtoumax(p, &q, 10);
		if (q == NULL || *q != '\0')
			len = UINTMAX_MAX;
	}
	if (http_GetHdr(ctx->http_req, hdr_defer, &p)) {
		if (len != UINTMAX_MAX || type == TUS_FINAL) {
			r->s.status = 400;
			return (0);
		}
		if (strcmp(p, "1") != 0) {
			r->s.status = 409; // or 400 ?
			return (0);
		}
	}

	if (len != UINTMAX_MAX) {
		if (fdisk->upload_length == -1)
			fdisk->upload_length = (ssize_t)len;
		else if ((ssize_t)len != fdisk->upload_length) {
			r->s.status = 409; // or 400 ?
			return (0);
		}
	}

	if (type == TUS_FINAL) {
		if (m != POST) {
			r->s.status = 403;
			tus_file_unlock(&r->fcore);
			AZ(r->fcore);
			return (0);
		}
		return (tus_request_complete(ctx, tussrv, r->fcore->ptr, fdisk));
	}

	assert (type != TUS_FINAL);

	/*lint -e{788} enums not used */
	switch (m) {
	case PATCH: {
		if (! ct_ok) {
			r->s.status = 415;
			return (0);
		}
		if ((ssize_t)off != fdisk->upload_offset) {
			r->s.status = 409;
			return (0);
		}
		if (fdisk->location_length > 0) {
			r->s.status = 204;	/* done file */
			return (0);
		}
		r->s.status = tus_body_to_file(ctx, r->fcore);
		break;
	}
	case POST: {
		cl = 0;
		if (http_GetHdr(ctx->http_req, H_Content_Length, &p))
			cl = strtoumax(p, &q, 10);
		else if (http_GetHdr(ctx->http_req, H_Transfer_Encoding, &p) &&
		    strcasecmp(p, "chunked") == 0)
			cl = 1;

		if (cl && ! ct_ok) {
			r->s.status = 415;
			return (0);
		}
		AZ(fdisk->upload_offset);
		if (cl == 0) {
			r->s.status = 201;
			break;
		}

		r->s.status = tus_body_to_file(ctx, r->fcore);
		if (r->s.status != 204)
			break;
		r->s.status = 201;
		break;
	}
	default:
		INCOMPL();
	}

	if (r->s.status == 413) {
		tus_file_del(&r->fcore);
		AZ(r->fcore);
		return (0);
	}
	if (fdisk->upload_offset == fdisk->upload_length) {
		tus_file_complete(r->fcore);
		AZ(pthread_cond_signal(&r->fcore->cond));
		if (type == TUS_PARTIAL)
			return (0);
		assert (type == TUS_SINGLE);
		c = tus_body_single(ctx->req, r->fcore);
		if (c == NULL) {
			VRT_fail(ctx, "not enough workspace");
			return (0);
		}
		return (tus_request_complete(ctx, tussrv, c, fdisk));
	}
	return (0);
}
