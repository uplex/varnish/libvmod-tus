/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>

#include <cache/cache_varnishd.h>
#include <vrnd.h>

#include "tus_b64.h"
#include "tus_file.h"
#include "tus_blob.h"
#include "tus_hdr.h"
#include "tus_hex.h"
#include "tus_file_exp.h"

VSPLAY_GENERATE(tus_files, tus_file_core, entry, tus_file_cmp)

#define TUS_FILE_PFX  "tus_"
#define TUS_FILE_RAND 16
#define TUS_FILE_SUFF "_XXXXXX"

//lint -esym(459, header_size) unprotected access
static uintmax_t header_size;
static unsigned tus_file_srvref(struct tus_file_core *);
static unsigned tus_file_unref(struct tus_file_core *);
static void tus_touch(const struct tus_file_core *, VCL_DURATION);

#define Lck_AssertUnlocked(a) AZ(Lck__Held(a))

/*
 * ------------------------------------------------------------
 * specific to TUS_CONCAT
 */
#include "tus_concat.h"

/*
 * the the list of concats is just a list of strings terminated by a zero-length
 * string
 */

static struct vsb *
tus_file_final_concat_parse(const char *spec)
{
	const char *p;
	struct vsb *vsb;

	vsb = VSB_new_auto();

	/* extract upload_path */
	while (spec != NULL) {
		while (*spec == ' ')
			spec++;
		if (TOK(spec, "http")) {
			AN(spec);
			if (*spec == 's')
				spec++;
			if (! TOK(spec, "://"))
				goto err;
			p = strchr(spec, '/');
			if (p == NULL)
				goto err;
			spec = p;
		}
		AN(spec);
		if (*spec != '/')
			goto err;
		p = strchr(spec, ' ');
		if (p == NULL)
			p = strchr(spec, '\0');
		AN(p);
		AZ(VSB_bcat(vsb, spec, p - spec));
		AZ(VSB_putc(vsb, '\0'));
		spec = strchr(spec, ' ');
	}
	AZ(VSB_putc(vsb, '\0'));
	AZ(VSB_finish(vsb));

	return (vsb);

  err:
	VSB_destroy(&vsb);
	return (NULL);
}

struct concat_embryo *
tus_file_final_concat(struct VPFX(tus_server) *srv,
    struct concat_embryo *embryo, const char *spec)
{
	struct tus_file_core *part, *parts[MAX_CONCAT] = {NULL};
	struct tus_concat *concat;
	const struct tus_file_disk *pdisk;
	struct vsb *vsb;
	unsigned i, n = 0;
	ssize_t length = 0;
	size_t l, ml;

	vsb = tus_file_final_concat_parse(spec);
	if (vsb == NULL)
		return (NULL);
	spec = VSB_data(vsb);

	tus_server_lock(srv);
	while ((l = strlen(spec)) > 0) {
		part = tus_file_lookup(srv, spec);
		if (part == NULL)
			goto lookup_err;
		pdisk = part->disk;
		if (pdisk == NULL ||
		    pdisk->type != TUS_PARTIAL)
			goto lookup_err;
		(void) tus_file_srvref(part);
		parts[n++] = part;
		if (n == MAX_CONCAT)
			goto lookup_err;
		spec += l;
		spec++;
	}
	tus_server_unlock(srv);

	if (n == 0)
		goto err;

	for (i = 0; i < n; i++) {
		part = parts[i];
		AN(part);
		pdisk = part->disk;
		AN(pdisk);
		if (part->ptr != NULL) {
			assert(TFCP_READABLE(part->ptr_type));
			length += pdisk->upload_length;
			continue;
		}

		Lck_Lock(&part->mtx);
		errno = EINTR;
		while (part->ptr == NULL && errno == EINTR) {
			errno = Lck_CondWaitTimeout(&part->cond,
			    &part->mtx, (vtim_dur)READY_TIMEOUT);
		}
		Lck_Unlock(&part->mtx);
		if (part->ptr == NULL)
			goto err;
		assert(TFCP_READABLE(part->ptr_type));
		length += pdisk->upload_length;
	}

	l = n * sizeof(struct tus_file_core *);
	ml = sizeof(*concat) + l;
	concat = malloc(ml);
	AN(concat);
	INIT_OBJ(concat, TUS_CONCAT_MAGIC);
	concat->n = n;
	memcpy(concat->cores, parts, l);

	INIT_OBJ(embryo, CONCAT_EMBRYO_MAGIC);
	embryo->srv = srv;
	embryo->spec_vsb = vsb;
	embryo->concat = concat;
	embryo->concat_l = ml;
	embryo->upload_length = length;

	return (embryo);

  lookup_err:
	tus_server_unlock(srv);
  err:
	for (i = 0; i < n; i++)
		(void) tus_file_unref(parts[i]);

	VSB_destroy(&vsb);
	return (NULL);
}

void
tus_file_final_birth(struct tus_file_core **fcorep,
    struct concat_embryo *embryo)
{
	struct tus_file_core *fcore;
	struct tus_file_disk *fdisk;
	struct vsb *vsb;
	ssize_t l;

	TAKE_OBJ_NOTNULL(fcore, fcorep, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertHeld(&fcore->mtx);
	CHECK_OBJ_NOTNULL(embryo, CONCAT_EMBRYO_MAGIC);
	fdisk = fcore->disk;
	CHECK_OBJ_NOTNULL(fdisk, VMOD_TUS_FILE_DISK_MAGIC);

	assert(fcore->ptr_type == TFCP_INVALID);
	AZ(fcore->ptr);
	AZ(fcore->len);
	fcore->ptr = embryo->concat;
	fcore->len = embryo->concat_l;
	fcore->ptr_type = TFCP_CONCAT;
	fdisk->upload_length = fdisk->upload_offset = embryo->upload_length;
	vsb = embryo->spec_vsb;

	assert(fcore->fd >= 0);
	l = VSB_len(vsb);
	assert(l >= 0);
	if (write(fcore->fd, VSB_data(vsb), (size_t)l) < 0)
		tus_file_del(&fcore);
	VSB_destroy(&vsb);
	memset(embryo, 0, sizeof *embryo);
	*fcorep = fcore;
}

static void
tus_file_concat_fini(struct tus_concat *concat)
{
	unsigned i;

	for (i = 0; i < concat->n; i++)
		(void) tus_file_unref(concat->cores[i]);

	free(concat);
}

void
tus_file_final_abort(struct concat_embryo *embryo)
{

	CHECK_OBJ_NOTNULL(embryo, CONCAT_EMBRYO_MAGIC);
	tus_file_concat_fini(embryo->concat);
	VSB_destroy(&embryo->spec_vsb);
	memset(embryo, 0, sizeof *embryo);
}

static void
tus_file_final_fini(struct tus_file_core *fcore)
{
	struct tus_concat *concat;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertHeld(&fcore->mtx);

	if (fcore->ptr == NULL)
		return;

	assert(fcore->ptr_type == TFCP_CONCAT);
	CAST_OBJ_NOTNULL(concat, fcore->ptr, TUS_CONCAT_MAGIC);
	fcore->ptr = NULL;
	fcore->ptr_type = TFCP_INVALID;
	assert (fcore->len >= sizeof *concat);
	fcore->len = 0;

	tus_file_concat_fini(concat);
}

// checksum over all parts
VCL_BLOB
tus_concat_hash(VRT_CTX, const struct VPFX(tus_server) *srv,
    const struct tus_concat *concat)
{
	struct vmod_blobdigest_digest *d;
	struct tus_chksum *c;
	struct tus_file_core *part;
	unsigned i;

	CHECK_OBJ_NOTNULL(concat, TUS_CONCAT_MAGIC);

	d = tus_server_digest(srv);
	if (d == NULL)
		return (NULL);

	c = tus_chksum_new(ctx, d);
	if (c == NULL)
		return (NULL);

	for (i = 0; i < concat->n; i++) {
		part = concat->cores[i];
		if (part->len == 0)
			continue;
		assert(TFCP_READABLE(part->ptr_type));
		AN(part->ptr);
		tus_chksum_update(ctx, c, part->ptr, part->len);
	}
	return (tus_chksum_final(ctx, c));
}

// all parts url
const char *
tus_file_final_urls(VRT_CTX, const struct tus_file_core *fcore, const char *pfx)
{
	const struct tus_file_core *part;
	const struct tus_file_disk *fdisk;
	const struct tus_concat *concat;
	struct vsb vsb[1];
	unsigned i;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertHeld(&fcore->mtx);

	if (fcore->ptr == NULL)
		return (NULL);

	WS_VSB_new(vsb, ctx->ws);

	/*
	 * ignoring VSB_*() returns because ws can overflow
	 * the error is returned by WS_VSB_finish
	 */
	(void)VSB_cat(vsb, "final;");
	assert(fcore->ptr_type == TFCP_CONCAT);
	CAST_OBJ_NOTNULL(concat, fcore->ptr, TUS_CONCAT_MAGIC);
	for (i = 0; i < concat->n; i++) {
		part = concat->cores[i];
		fdisk = part->disk;
		CHECK_OBJ_NOTNULL(fdisk, VMOD_TUS_FILE_DISK_MAGIC);
		if (i > 0)
			(void)VSB_cat(vsb, " ");
		(void)VSB_cat(vsb, pfx);
		(void)VSB_cat(vsb, fdisk->upload_path);
	}
	return (WS_VSB_finish(vsb, ctx->ws, NULL));
}

/*
 * ------------------------------------------------------------
 * generic files
 */

/* ensure file is open */
static int
tus_file_open(struct tus_file_core *fcore)
{
	int basefd;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertHeld(&fcore->mtx);

	if (fcore->fd < 0) {
		basefd = tus_server_basefd(fcore->server);
		fcore->fd = openat(basefd, fcore->filename,
		    O_RDWR | O_CLOEXEC | O_APPEND);
	}

	return (fcore->fd);
}

/* close */
static void
tus_file_close(struct tus_file_core *fcore)
{
	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertHeld(&fcore->mtx);

	if (fcore->fd == -1)
		return;

	(void) close(fcore->fd);
	fcore->fd = -1;
}

static unsigned tus_file_unref_locked(struct tus_file_core *fcore);

void
tus_file_init(void)
{
	size_t sps;
	int ps;

	ps = getpagesize();
	assert(ps > 0);
	sps = (unsigned)ps;

	header_size = RUP2(sizeof(struct tus_file_disk), sps);
	assert(header_size >= sizeof(struct tus_file_disk));
}

/* returns NULL if exists */
static struct tus_file_core *
tus_file_core_new(struct VPFX(tus_server) *srv,
    int fd, const char *filename, struct tus_file_disk *fdisk)
{
	struct tus_file_core *fcore;

	tus_server_AssertLocked(srv);

	ALLOC_OBJ(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	AN(fcore);
	fcore->server = srv;
	fcore->fd = fd;
	REPLACE(fcore->filename, filename);
	Lck_New(&fcore->mtx, lck_fcore);
	AZ(pthread_cond_init(&fcore->cond, NULL));
	fcore->disk = fdisk;
	fcore->srvref = 1;

	if (VSPLAY_INSERT(tus_files, tus_server_files(srv), fcore) == NULL) {
		tus_exp_insert(fcore);
		return (fcore);
	}

	Lck_Delete(&fcore->mtx);
	AZ(pthread_cond_destroy(&fcore->cond));
	REPLACE(fcore->filename, NULL);
	FREE_OBJ(fcore);
	return (NULL);
}

static void *
tus_mmap_header(int fd)
{
	return (mmap(NULL, header_size, PROT_READ | PROT_WRITE,
	    MAP_SHARED | MAP_NORESERVE, fd, (off_t)0));
}

static void
tus_file_mmap(struct tus_file_core *fcore);
static char * const empty = "";

void
tus_file_complete(struct tus_file_core *fcore)
{
	struct tus_file_disk *fdisk;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertHeld(&fcore->mtx);
	fdisk = fcore->disk;
	CHECK_TUS_FILE_DISK(fdisk);

	assert(fdisk->upload_offset == fdisk->upload_length);

	if (fdisk->upload_offset > 0) {
		tus_file_mmap(fcore);
		return;
	}

	AZ(fdisk->upload_length);
	AZ(fdisk->upload_offset);
	AZ(fcore->ptr);
	AZ(fcore->len);
	assert(fcore->ptr_type == TFCP_INVALID);
	fcore->ptr_type = TFCP_EMPTY;
	fcore->ptr = empty;
}

static void
tus_file_mmap(struct tus_file_core *fcore)
{
	struct tus_file_disk *fdisk;
	int fd, prot = PROT_READ;
	off_t len;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertHeld(&fcore->mtx);
	fdisk = fcore->disk;
	CHECK_TUS_FILE_DISK(fdisk);

	if (fcore->ptr != NULL) {
		assert(fcore->ptr_type >= TFCP_MMAP_RO);
		return;
	}

	AZ(fcore->ptr);
	AZ(fcore->len);
	assert(fcore->ptr_type == TFCP_INVALID);
	fd = tus_file_open(fcore);

	assert(fdisk->upload_offset > 0);
	assert(fdisk->upload_length > 0);
	assert(fdisk->upload_offset <= fdisk->upload_length);
	if (fdisk->upload_offset < fdisk->upload_length) {
		len =  (off_t)header_size;
		len += fdisk->upload_length;
		prot |= PROT_WRITE;
		// XXX error handling
		AZ(fallocate(fd, (int)0, (off_t)0, len));
	}

	fcore->ptr = mmap(NULL, (size_t)fdisk->upload_length, prot,
	    MAP_SHARED | MAP_NORESERVE, fd, (off_t)header_size);
	AN(fcore->ptr);
	fcore->ptr_type = (prot & PROT_WRITE) ?
		TFCP_MMAP_RW : TFCP_MMAP_RO;
	tus_file_close(fcore);
	fcore->len = (size_t)fdisk->upload_length;
}

static void
tus_file_disk_del(struct tus_file_disk **fdiskp,
    int *fdp, const char *filename, int basefd)
{
	if (fdiskp != NULL) {
		if (*fdiskp != NULL)
			(void) munmap(*fdiskp, header_size);
		*fdiskp = NULL;
	}

	if (fdp != NULL) {
		if (*fdp >= 0)
			(void) close(*fdp);
		*fdp = -1;
	}
	if (filename != NULL) {
		if (basefd >= 0)
			(void) unlinkat(basefd, filename, 0);
		else
			(void) unlink(filename);
	}
}

/* fini an fcore already removed from tus_files and without references */
static void
tus_file_fini(struct tus_file_core *fcore)
{
	struct tus_file_disk *fdisk;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertUnlocked(&fcore->mtx);

	fdisk = fcore->disk;
	CHECK_TUS_FILE_DISK(fdisk);
	AZ(fcore->srvref);
	AZ(fcore->ref);

	if (fdisk->type == TUS_FINAL) {
		tus_file_final_fini(fcore);
	} else if (fcore->ptr != NULL && fcore->ptr_type >= TFCP_MMAP_RO) {
		fcore->ptr_type = TFCP_INVALID;
		(void) munmap(fcore->ptr, fcore->len);
		fcore->ptr = NULL;
		fcore->len = 0;
	} else if (fcore->ptr != NULL && fcore->ptr_type == TFCP_EMPTY) {
		fcore->ptr_type = TFCP_INVALID;
		fcore->ptr = NULL;
		fcore->len = 0;
	}
	assert(fcore->ptr_type == TFCP_INVALID);
	AZ(fcore->ptr);
	AZ(fcore->len);

	fcore->disk = NULL;
	tus_file_disk_del(&fdisk, &fcore->fd, fcore->filename,
	    tus_server_basefd(fcore->server));
	AZ(fdisk);
	assert(fcore->fd == -1);

	Lck_Delete(&fcore->mtx);
	AZ(pthread_cond_destroy(&fcore->cond));
	REPLACE(fcore->filename, NULL);
	FREE_OBJ(fcore);
}

/* try to lock the file, losing the pointer to it if it failes */
int
tus_file_trylock(struct tus_file_core **fcorep)
{
	int err;
	struct tus_file_core *fcore;

	AN(fcorep);
	fcore = *fcorep;
	if (fcore == NULL)
		return (EINVAL);

	CHECK_OBJ(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	do {
		err = Lck_Trylock(&fcore->mtx);
		if (err == 0)
			return (err);
	} while (err == EINTR);

	*fcorep = NULL;
	return (err);
}

/* keep a reference across requests, but done with it for this request */
void
tus_file_unlock(struct tus_file_core **fcorep)
{
	struct tus_file_core *fcore;

	TAKE_OBJ_NOTNULL(fcore, fcorep, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_Unlock(&fcore->mtx);
}

/* under both fcore and server mtx */
static inline unsigned
ref_transfer(struct tus_file_core *fcore)
{
	unsigned r;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	tus_server_AssertLocked(fcore->server);
	Lck_AssertHeld(&fcore->mtx);

	r = fcore->srvref;
	fcore->srvref = 0;
	assert(r <= INT_MAX);

	fcore->ref += (int)r;
	assert(fcore->ref > 0);
	return (r);
}

/* caller holds tus_server mtx */
static void
tus_file_del_shutdown(struct tus_file_core **fcorep)
{
	struct tus_file_core *fcore;
	const struct tus_file_core *rm;

	TAKE_OBJ_NOTNULL(fcore, fcorep, VMOD_TUS_FILE_CORE_MAGIC);
	tus_server_AssertLocked(fcore->server);
	Lck_AssertHeld(&fcore->mtx);

	AN(ref_transfer(fcore));
	rm = VSPLAY_REMOVE(tus_files, tus_server_files(fcore->server),
	    fcore);
	assert (rm == fcore);
	tus_exp_delete(fcore);

	AZ(tus_file_unref_locked(fcore));
}

static inline void
tus_file_remove(struct tus_file_core *fcore)
{
	struct VPFX(tus_server) *srv;
	const struct tus_file_core *rm;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);

	srv = fcore->server;

	tus_server_lock(srv);
	if (ref_transfer(fcore)) {
		rm = VSPLAY_REMOVE(tus_files, tus_server_files(srv), fcore);
		assert (rm == fcore);
	}
	tus_server_unlock(srv);
}

/* called from user under lock */
void
tus_file_del(struct tus_file_core **fcorep)
{

	AN(fcorep);
	CHECK_OBJ_NOTNULL(*fcorep, VMOD_TUS_FILE_CORE_MAGIC);

	tus_file_remove(*fcorep);
	tus_touch(*fcorep, (VCL_DURATION)0);
	tus_file_unlock(fcorep);
}

/* called from expiry */
void
tus_file_exp(struct tus_file_core **fcorep)
{
	struct tus_file_core *fcore;

	TAKE_OBJ_NOTNULL(fcore, fcorep, VMOD_TUS_FILE_CORE_MAGIC);

	Lck_Lock(&fcore->mtx);
	tus_file_remove(fcore);
	(void) tus_file_unref_locked(fcore);
}

/* close all files for tus_server shutdown */
void
tus_file_shutdown(struct VPFX(tus_server) *srv)
{
	struct tus_file_core *fcore, *next;
	struct tus_files *files = tus_server_files(srv);

	tus_server_lock(srv);
	fcore = VSPLAY_MIN(tus_files, files);
	while (fcore != NULL) {
		next = VSPLAY_NEXT(tus_files, files, fcore);
		REPLACE(fcore->filename, NULL); // prevent unlink
		Lck_Lock(&fcore->mtx);
		tus_file_del_shutdown(&fcore);
		AZ(fcore);
		fcore = next;
	}
	tus_server_unlock(srv);
}

/* under server mutex */
static unsigned
tus_file_srvref(struct tus_file_core *fcore)
{
	unsigned r;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	tus_server_AssertLocked(fcore->server);
	r = fcore->srvref++;
	// as long as the object can be found, it has one srvref at least
	assert(r > 0);
	return (r);
}

#ifdef UNUSED
static unsigned
tus_file_ref(struct tus_file_core *fcore)
{
	int r;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_Lock(&fcore->mtx);
	r = fcore->ref++ + fcore->srvref;
	Lck_Unlock(&fcore->mtx);
	assert(r >= 0);
	return (r);
}
#endif

static unsigned
tus_file_unref_locked(struct tus_file_core *fcore)
{
	int r;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_AssertHeld(&fcore->mtx);

	r = --fcore->ref;
	Lck_Unlock(&fcore->mtx);
	r += (int)fcore->srvref;
	assert (r >= 0);
	if (r == 0)
		tus_file_fini(fcore);
	return ((unsigned)r);
}

static unsigned
tus_file_unref(struct tus_file_core *fcore)
{

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	Lck_Lock(&fcore->mtx);
	return (tus_file_unref_locked(fcore));
}

/* under tus_server mtx to protect tus_files */
static void
tus_file_add(struct VPFX(tus_server) *srv, int basefd, const char *filename)
{
	struct tus_file_core *fcore = NULL;
	struct tus_file_disk *fdisk = NULL;
	struct stat st;
	int fd = -1;
	const char *err;
	off_t sz;

	tus_server_AssertLocked(srv);

	if (fstatat(basefd, filename, &st, AT_SYMLINK_NOFOLLOW)) {
		fprintf(stderr, "tus add stat %s: %d\n", filename, errno);
		goto err;
	}

	assert(st.st_size >= 0);
	if ((uintmax_t)st.st_size < header_size) {
		fprintf(stderr, "tus add %s: too small\n", filename);
		goto err;
	}

	fd = openat(basefd, filename, O_RDWR | O_CLOEXEC | O_APPEND);
	if (fd < 0) {
		fprintf(stderr, "tus add open %s: %d\n", filename, errno);
		goto err;
	}

	fdisk = tus_mmap_header(fd);
	if (fdisk == NULL) {
		fprintf(stderr, "tus add mmap %s: %d\n", filename, errno);
		goto err;
	}

	err = tus_file_disk_err(fdisk);
	if (err != NULL) {
		fprintf(stderr, "tus add %s: %s\n", filename, err);
		goto err;
	}

	/*
	 * we do not read in final concats because they do not need relevant
	 * amounts of data for re-upload
	 */
	if (fdisk->type == TUS_FINAL) {
		fprintf(stderr, "tus add %s: is final\n", filename);
		goto err;
	}

	if (fdisk->upload_expires < VTIM_real()) {
		fprintf(stderr, "tus add %s: expired\n", filename);
		goto err;
	}

	// checked above
	assert(st.st_size >= (off_t)header_size);
	sz = st.st_size - (off_t)header_size;
	/*
	 * if data was written before we updated the offset,
	 * we truncate to avoid potentially corrupted data
	 */
	if (fdisk->location_length > 0) {
		// .done() called & truncated
	}
	else if (fdisk->upload_offset < sz) {
		AZ(ftruncate(fd, (off_t)header_size + fdisk->upload_offset));
		fprintf(stderr, "tus add %s: truncated to %zd\n", filename, sz);
	}
	else if (fdisk->upload_offset > sz) {
		fprintf(stderr, "tus add %s: updated offset from %zd to %zd\n",
			filename, fdisk->upload_offset, sz);
		fdisk->upload_offset = sz;
	}

	/* note: the close can cause a SIGSEGV if a file is unlinked
	 * concurrently. But as we should be the only user of the base
	 * directory, that should not happen
	 *
	 * closing makes sense because we are likely not to use all
	 * files present when starting
	 */

	fcore = tus_file_core_new(srv, fd, filename, fdisk);
	if (fcore != NULL) {
		Lck_Lock(&fcore->mtx);
		tus_file_close(fcore);
		Lck_Unlock(&fcore->mtx);
		return;
	}

	fprintf(stderr, "tus add %s: duplicate upload_path %s\n",
	    filename, fdisk->upload_path);

  err:
	tus_file_disk_del(&fdisk, &fd, filename, basefd);
	AZ(fdisk);
	assert(fd == -1);
}

/* under tus_server mtx to protect tus_files */
void
tus_file_load(struct VPFX(tus_server) *srv)
{
	int basefd = tus_server_basefd(srv);
	DIR *dir;
	struct dirent *ent;

	tus_server_AssertLocked(srv);

	dir = fdopendir(dup(basefd));
	AN(dir);
	while ((ent = readdir(dir)) != NULL) {
		if (STRNCMP(ent->d_name, TUS_FILE_PFX))
			continue;
		if (ent->d_type != DT_REG)
			continue;
		tus_file_add(srv, basefd, ent->d_name);
	}
	AZ(closedir(dir));
}

static void
tus_touch(const struct tus_file_core *fcore, VCL_DURATION expires)
{
	struct tus_file_disk *fdisk;
	VCL_TIME t = VTIM_real();

	fdisk = fcore->disk;
	CHECK_TUS_FILE_DISK(fdisk);

	if (fdisk->upload_expires < t)
		return;

	fdisk->upload_expires = t + expires;
	tus_exp_touch(fcore);
}

static inline void
tus_file_fdisk_init(struct tus_file_disk *fdisk)
{
	INIT_OBJ(fdisk, VMOD_TUS_FILE_DISK_MAGIC);
	fdisk->guard_magic = VMOD_TUS_FILE_DISK_MAGIC;
	fdisk->guard2_magic = VMOD_TUS_FILE_DISK_MAGIC;
	fdisk->guard3_magic = VMOD_TUS_FILE_DISK_MAGIC;
	fdisk->version = 2;
}

/* under tus_server mtx to protect tus_files */
struct tus_file_core *
tus_file_lookup(struct VPFX(tus_server) *srv, const char *upload_path)
{
	struct tus_file_core fcore, *found;
	struct tus_file_disk needle;
	size_t l = strlen(upload_path);

	if (l >= TUS_PATH_MAX) {
		errno = ENAMETOOLONG;
		return NULL;
	}

	INIT_OBJ(&fcore, VMOD_TUS_FILE_CORE_MAGIC);
	tus_file_fdisk_init(&needle);
	strcpy(needle.upload_path, upload_path);

	needle.upload_path_length = (unsigned)l;
	fcore.disk = &needle;

	found = VSPLAY_FIND(tus_files, tus_server_files(srv), &fcore);

	if (found == NULL)
		return (NULL);

	tus_touch(found, tus_server_expires(srv));

	return (found);
}

static void
tus_name_rnd(struct vsb *vsb)
{
	char rnd[TUS_FILE_RAND];
	struct vrt_blob b;

	AZ(VRND_RandomCrypto(rnd, sizeof rnd));
	b.type = 0x1055;
	b.blob = rnd;
	b.len = sizeof rnd;

	tus_vsbhex(vsb, &b);
}

/* under tus_server mtx to protect tus_files */
struct tus_file_core *
tus_file_new(VRT_CTX, struct VPFX(tus_server) *srv, enum tus_f_type type,
    const char *upload_path, const char *id, const char *metadata,
    unsigned *status, const char **reason)
{
	struct tus_file_core *fcore;
	struct tus_file_disk *fdisk = NULL;
	char buf[PATH_MAX];
	struct vsb vsb_path[1];
	const char *path;
	const char *filename;	// relative to base
	int fd;
	size_t l;

	AN(status);
	AN(reason);

	if (id != NULL) {
		while (*id == '/')
			id++;
		l = strlen(id);
	} else {
		// tus_name_rnd uses tus_hex
		l = strlen(TUS_FILE_PFX) + TUS_FILE_RAND * 2 +
		    strlen(TUS_FILE_SUFF);
	}
	l++;

	if (strlen(upload_path) + l > TUS_PATH_MAX) {
		errno = ENAMETOOLONG;
		VSLb(ctx->vsl, SLT_Error, "%s: path too long: %s",
		    tus_server_name(srv), upload_path);
		*status = 400;
		*reason = "Path too long";
		return (NULL);
	}

	if (metadata != NULL && strlen(metadata) + 1 > TUS_METADATA_MAX) {
		errno = ENAMETOOLONG;
		VSLb(ctx->vsl, SLT_Error, "%s: metadata too long: %s",
		    tus_server_name(srv), metadata);
		*status = 400;
		*reason = "Metadata too long";
		return (NULL);
	}

	/* there is no mkostempat() */
	AN(VSB_init(vsb_path, buf, sizeof buf));
	AZ(VSB_cat(vsb_path, tus_server_basedir(srv)));
	AZ(VSB_cat(vsb_path, "/" TUS_FILE_PFX));
	tus_name_rnd(vsb_path);
	AZ(VSB_cat(vsb_path, TUS_FILE_SUFF));
	AZ(VSB_finish(vsb_path));

	fd = mkostemp(VSB_data(vsb_path), O_APPEND | O_CLOEXEC);
	if (fd < 0) {
		VSLb(ctx->vsl, SLT_Error, "%s: mkostemp(%s) failed: %d (%s)",
		    tus_server_name(srv), VSB_data(vsb_path),
		    errno, strerror(errno));
		*status = 500;
		*reason = "mkostemp failed";
		return (NULL);
	}
	path = VSB_data(vsb_path);
	VSB_fini(vsb_path);

	if (fallocate(fd, (int)0, (off_t)0, (off_t)header_size)) {
		VSLb(ctx->vsl, SLT_Error, "%s: fallocate(%s) failed: %d (%s)",
		    tus_server_name(srv), path,
		    errno, strerror(errno));
		*status = 500;
		*reason = "fallocate failed";
		goto err;
	}

	filename = strrchr(path, '/');
	AN(filename);
	filename++;
	AN(*filename);

	if (id == NULL)
		id = filename;

	fdisk = tus_mmap_header(fd);

	if (fdisk == NULL) {
		VSLb(ctx->vsl, SLT_Error,
		    "%s: tus_mmap_header(%s) failed: %d (%s)",
		    tus_server_name(srv), path,
		    errno, strerror(errno));
		*status = 500;
		*reason = "mmap failed";
		goto err;
	}

	tus_file_fdisk_init(fdisk);

	fdisk->type = type;

	l = strlen(upload_path);
	AN(l);
	assert(l < TUS_PATH_MAX);

	fdisk->upload_path_length = (unsigned)l;
	strcpy(fdisk->upload_path, upload_path);
	if (upload_path[fdisk->upload_path_length - 1] != '/')
		fdisk->upload_path[fdisk->upload_path_length++] = '/';
	strcpy(fdisk->upload_path + fdisk->upload_path_length, id);
	l = strlen(id);
	assert(l < TUS_PATH_MAX);
	fdisk->upload_path_length += (unsigned)l;
	assert(fdisk->upload_path_length < TUS_PATH_MAX);

	if (metadata && *metadata != '\0') {
		l = strlen(metadata);
		assert(l < TUS_METADATA_MAX);
		fdisk->metadata_length = (unsigned)l;
		strcpy(fdisk->metadata, metadata);
	}

	fdisk->upload_length = -1;
	fdisk->upload_expires = VTIM_real() + tus_server_expires(srv);
	CHECK_TUS_FILE_DISK(fdisk);

	fcore = tus_file_core_new(srv, fd, filename, fdisk);

	if (fcore != NULL)
		return (fcore);

	/* undo. happens for clash with custom id */
	AN(id);
	*status = 409;
	*reason = "custom id clash";

  err:
	tus_file_disk_del(&fdisk, &fd, path, -1);
	return (NULL);
}

/* ------------------------------------------------------------
 * things all suckers have in common
 */

struct tus_suck_common {
	VRT_CTX;
	VCL_BYTES		max;
	ssize_t			*upload_offset;
	struct tus_chksum	*chksum;
};

struct tus_suck_fd {
	unsigned		magic;
#define TUS_SUCK_FD_MAGIC	0x10550c55
	int			fd;
	struct tus_suck_common	sc;
};

struct tus_suck_mmap {
	unsigned		magic;
#define TUS_SUCK_MMAP_MAGIC	0x11ffbafa
	void			*ptr;
	struct tus_suck_common	sc;
};

typedef int suck_truncate_f(void *, off_t);

struct tus_suck {
	union {
		struct tus_suck_fd	sfd;
		struct tus_suck_mmap	sm;
	} priv;
	objiterate_f	*func;
	suck_truncate_f	*trunc;
};

static int
tus_suck_finish(const struct tus_suck_common *sc,
    ssize_t written, const void *ptr, size_t len)
{
	assert(len > 0);
	if (written < 0 || (size_t)written != len)
		return (-1);

	assert((size_t)written == len);

	*sc->upload_offset += written;
	if (*sc->upload_offset > sc->max) {
		errno = EFBIG;
		return (-1);
	}

	if (sc->chksum)
		tus_chksum_update(sc->ctx, sc->chksum, ptr, len);

	return (0);
}

/* ------------------------------------------------------------
 * suck to an fd (data is not mmaped)
 */

static int v_matchproto_(objiterate_f)
tus_suck_fd_f(void *priv, unsigned flush, const void *ptr, ssize_t len)
{
	struct tus_suck_fd *sfd;

	CAST_OBJ_NOTNULL(sfd, priv, TUS_SUCK_FD_MAGIC);

	// we could use writev(), but unclear if it is worth it
	(void) flush;

	if (len == 0)
		return (0);

	assert(len > 0);

	return (tus_suck_finish(
			&sfd->sc,
			write(sfd->fd, ptr, (size_t)len),
			ptr, (size_t)len));
}

static int
tus_suck_fd_truncate_f(void *priv, off_t length)
{
	struct tus_suck_fd *sfd;

	CAST_OBJ_NOTNULL(sfd, priv, TUS_SUCK_FD_MAGIC);

	return (ftruncate(sfd->fd, (off_t)header_size + length));
}

static inline struct tus_suck_common *
tus_suck_fd_init(struct tus_suck *suck, struct tus_file_core *fcore)
{
	struct tus_suck_fd *sfd;

	sfd = &suck->priv.sfd;
	sfd->magic = TUS_SUCK_FD_MAGIC;
	sfd->fd = tus_file_open(fcore);
	assert(sfd->fd >= 0);
	suck->func = tus_suck_fd_f;
	suck->trunc = tus_suck_fd_truncate_f;
	return (&sfd->sc);
}

/* ------------------------------------------------------------
 * suck to mmapped
 */

static int v_matchproto_(objiterate_f)
tus_suck_mmap_f(void *priv, unsigned flush, const void *ptr, ssize_t len)
{
	struct tus_suck_mmap *sm;
	const struct tus_suck_common *sc;
	void *wptr;

	CAST_OBJ_NOTNULL(sm, priv, TUS_SUCK_MMAP_MAGIC);
	sc = &sm->sc;

	(void) flush;

	if (len == 0)
		return (0);

	assert(len > 0);

	if (*sc->upload_offset + len > sc->max) {
		errno = EFBIG;
		return (-1);
	}

	wptr = (char *)sm->ptr + *sc->upload_offset;
	memcpy(wptr, ptr, (size_t)len);
	/* XXX write ordering with fdisk flush ?
	 *
	 * maybe we can just truncate all trailing NULs
	 * when reading?
	 */

	return (tus_suck_finish(sc, len, ptr, (size_t)len));
}

static int
tus_suck_mmap_truncate_f(void *priv, off_t length)
{
	struct tus_suck_mmap *sm;
	const struct tus_suck_common *sc;
	void *wptr;
	size_t sz;

	CAST_OBJ_NOTNULL(sm, priv, TUS_SUCK_MMAP_MAGIC);
	sc = &sm->sc;

	assert(length >= 0);
	assert(length <= sc->max);
	if (length >= *sc->upload_offset)
		return (0);
	// length < upload_offset

	assert(*sc->upload_offset >= 0);

	wptr = (char *)sm->ptr + length;
	sz  = (size_t)*sc->upload_offset;
	sz -= (size_t)length;

	memset(wptr, 0, sz);
	return (0);
}

static inline struct tus_suck_common *
tus_suck_mmap_init(struct tus_suck *suck, struct tus_file_core *fcore)
{
	struct tus_suck_mmap *sm = &suck->priv.sm;

	tus_file_mmap(fcore);

	sm->magic = TUS_SUCK_MMAP_MAGIC;
	sm->ptr = fcore->ptr;
	AN(sm->ptr);

	suck->func = tus_suck_mmap_f;
	suck->trunc = tus_suck_mmap_truncate_f;
	return (&sm->sc);
}

/* ------------------------------------------------------------
 * top level sucking
 */

static inline void
tus_suck_trunc(struct tus_suck *suck, off_t length)
{
	if (suck->trunc == NULL)
		return;
	AZ(suck->trunc(&suck->priv, length));
}

static inline struct tus_suck_common *
tus_suck_init(struct tus_suck *suck, struct tus_file_core *fcore,
    struct tus_file_disk *fdisk)
{
	VCL_BYTES bm;

	bm = tus_server_multipart(fcore->server);

	if (bm > 0 && fdisk->upload_offset >= bm)
		return (tus_suck_mmap_init(suck, fcore));
	else
		return (tus_suck_fd_init(suck, fcore));
}

unsigned
tus_body_to_file(VRT_CTX, struct tus_file_core *fcore)
{
	struct tus_suck suck;
	struct tus_suck_common *sc;
	const char *hdr;
	ssize_t offset_saved, written;
	struct tus_file_disk *fdisk;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->req, REQ_MAGIC);
	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);

	fdisk = fcore->disk;
	CHECK_OBJ_NOTNULL(fdisk, VMOD_TUS_FILE_DISK_MAGIC);

	memset(&suck, 0, sizeof suck);

	sc = tus_suck_init(&suck, fcore, fdisk);
	AN(sc);

	offset_saved = fdisk->upload_offset;
	sc->ctx = ctx;
	sc->max = tus_server_max(fcore->server);

	if (fdisk->upload_length != -1 &&
	    fdisk->upload_length < sc->max)
		sc->max = fdisk->upload_length;

	sc->upload_offset = &fdisk->upload_offset;

	if (http_GetHdr(ctx->http_req, hdr_chksum, &hdr)) {
		sc->chksum = tus_chksum_hdr(ctx, hdr);
		if (sc->chksum == NULL)
			return (400);
	}

	written = VRB_Iterate(ctx->req->wrk, ctx->vsl, ctx->req,
	    suck.func, &suck.priv);

	if (written < 0 && errno == EFBIG) {
		tus_suck_trunc(&suck, sc->max);
		fdisk->upload_offset = fdisk->upload_length = sc->max;
		return (413);
	}

	if (sc->chksum == NULL) {
		if (written >= 0)
			return (204);
		else
			return (408);	// Timeout
	}

	if (tus_chksum_equal(ctx, sc->chksum))
			return (204);

	tus_suck_trunc(&suck, offset_saved);
	fcore->disk->upload_offset = offset_saved;
	return (460);
}

VCL_BOOL
tus_file_done(struct tus_file_core *fcore, struct tus_file_disk *fdisk,
    const char *url)
{
	int fd, need_truncate;
	size_t l;

	if (url == NULL)
		return (0);

	l = strlen(url);
	if (l == 0 || l >= TUS_PATH_MAX)
		return (0);

	need_truncate = (fdisk->location_length == 0);
	strcpy(fdisk->location, url);
	fdisk->location_length = (unsigned)l;

	/*
	 * for TUS_FINAL files, we store the list of parts in the data section,
	 * so we keep it intact
	 *
	 * for TUS_SINGLE files, we delete the data because we now have it at
	 * location
	 */

	if (fdisk->type == TUS_FINAL || need_truncate == 0)
		return (1);

	assert(fdisk->type == TUS_SINGLE);

	fd = tus_file_open(fcore);
	if (fd >= 0) {
		AZ(ftruncate(fd, (off_t)header_size));
		tus_file_close(fcore);
	}
	return (1);
}

void
tus_file_sync(struct tus_file_core *fcore)
{
	if (fcore == NULL)
		return;

	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);

	if (fcore->fd < 0)
		return;
	AZ(fdatasync(fcore->fd));
}

static inline const char *
tus_meta_find(const char *meta, const char *key, size_t l,
    const char **vp, size_t *vlp)
{
	const char *v, *ve;

	while (meta != NULL && *meta != '\0') {
		if (strncmp(meta, key, l) == 0) {
			v = meta + l;
			if (*v == '\0' || *v == ',' ||
			    (v[0] == ' ' && v[1] == ','))
				return (meta);
			if (*v == ' ') {
				v++;
				if (vp != NULL)
					*vp = v;
				if (vlp != NULL) {
					ve = strchr(v, ',');
					if (ve != NULL) {
						assert(ve >= v);
						*vlp = (size_t)(ve - v);
					}
					else
						*vlp = strlen(v);
				}
				return (meta);
			}
		}

		meta = strchr(meta, ',');
		if (meta != NULL)
			meta++;
	}
	return (NULL);
}

VCL_BOOL
tus_file_meta(VRT_CTX, const struct tus_file_disk *fdisk,
    const char *k, VCL_BLOB *b)
{
	const char *meta, *v = NULL;
	size_t kl = strlen(k), vl = 0;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(fdisk, VMOD_TUS_FILE_DISK_MAGIC);

	meta = fdisk->metadata;

	if (b == NULL)
		return (tus_meta_find(meta, k, kl, NULL, NULL) != NULL);

	meta = tus_meta_find(meta, k, kl, &v, &vl);
	if (meta == NULL)
		return (0);

	*b = tus_b64_decode(ctx, v, (VCL_INT)vl);

	return (1);
}
