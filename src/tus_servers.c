/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <cache/cache.h>

#include "tus_file.h"
#include "tus_servers.h"

struct tus_servers tus_servers[1] = { VSPLAY_INITIALIZER(tus_servers) };

VSPLAY_GENERATE(tus_servers, VPFX(tus_server), entry, tus_server_cmp)

const char *
tus_server_name(const struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->vcl_name);
}

struct tus_files *
tus_server_files(struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->files);
}

int
tus_server_basefd(const struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->basefd);
}

const char *
tus_server_basedir(const struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->basedir);
}

VCL_DURATION
tus_server_expires(const struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->expires);
}

void
tus_server_lock(struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	Lck_Lock(&s->mtx);
}

void
tus_server_unlock(struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	Lck_Unlock(&s->mtx);
}

void
tus_server__assertlocked(struct VPFX(tus_server) *s, const char *func,
    const char *file, int line)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	if (! Lck__Held(&s->mtx))
		VAS_Fail(func, file, line, "Lck__Held(&s->mtx)", VAS_ASSERT);
	if (! Lck__Owned(&s->mtx))
		VAS_Fail(func, file, line, "Lck__Owned(&s->mtx)", VAS_ASSERT);
}

VCL_BYTES
tus_server_max(const struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->max);
}

VCL_BYTES
tus_server_multipart(const struct VPFX(tus_server) *s)
{

	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->multipart);
}

struct vmod_blobdigest_digest *
tus_server_digest(const struct VPFX(tus_server) *s)
{
	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->digest);
}

struct tus_exp *
tus_server_exp(const struct VPFX(tus_server) *s)
{
	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->exp);
}

const char *
tus_server_schemeauth(const struct VPFX(tus_server) *s)
{
	CHECK_OBJ_NOTNULL(s, VMOD_TUS_SERVER_MAGIC);
	return (s->schemeauth);
}
