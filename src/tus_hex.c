/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <limits.h>

#include "cache/cache.h"
#include "vsb.h"

#include "tus_hex.h"
// ------------------------------------------------------------
// not using vmod_blob because upload filenames should work without it

static inline char
hexnibble(unsigned char n)
{
	unsigned char c;
	assert(n <= 0xf);
	c = n < 0xa ? '0' + n : 'a' + (n - 0xa);
	assert(c <= 'f');
	return ((char)c);
}

static VCL_STRING
tus_hex_buf(char * buf, size_t bufl, VCL_BLOB b)
{
	VCL_STRING r = buf;
	const unsigned char *p;
	unsigned char n;
	size_t l;

	if (b == NULL || b->len == 0)
		return ("");

	p = b->blob;
	l = b->len;
	while (l > 0) {
		if (bufl < 3)
			return (NULL);
		n = *p;
		buf[1] = hexnibble(n & 0xf);
		n >>= 4;
		buf[0] = hexnibble(n & 0xf);
		buf += 2;
		bufl -= 2;
		p++;
		l--;
	}
	AN(bufl);
	buf[0] = '\0';
	return (r);
}

VCL_STRING
tus_hex(VRT_CTX, VCL_BLOB b)
{
	char * buf;
	size_t bufl;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	if (b == NULL || b->len == 0)
		return ("");

	bufl = b->len * 2 + 1;
	assert(bufl <= UINT_MAX);
	buf = WS_Alloc(ctx->ws, (unsigned)bufl);
	if (buf == NULL)
		return (NULL);

	return (tus_hex_buf(buf, bufl, b));
}

void
tus_vsbhex(struct vsb *vsb, VCL_BLOB b)
{
	size_t l = b->len * 2 + 1;
	char buf[l];
	AZ(VSB_cat(vsb, tus_hex_buf(buf, l, b)));
}
