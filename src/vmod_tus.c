/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <stdlib.h>

#include <cache/cache.h>
#include <vcl.h>
#include <vrt_obj.h>

#include "vcc_tus_if.h"

#include "tus_file.h"
#include "tus_servers.h"
#include "tus_response.h"
#include "tus_request.h"
#include "tus_blob.h"
#include "tus_file_exp_setup.h"

/* ============================================================
 * vmod shared object globals
 */

static unsigned refcnt = 0;
static struct vsc_seg  *vsc_seg_lck = NULL;
struct VSC_lck *lck_fcore = NULL;
static struct VSC_lck *lck_tussrv = NULL;

/* ============================================================
 * vmod init/fini
 */

static int
tus_init(VRT_CTX, struct vmod_priv *priv)
{
	(void) priv;

	if (refcnt++ > 0)
		return (0);

	AZ(vsc_seg_lck);
	AZ(lck_fcore);
	lck_fcore = Lck_CreateClass(&vsc_seg_lck, "tus.fcore");
	AN(lck_fcore);
	AZ(lck_tussrv);
	lck_tussrv = Lck_CreateClass(&vsc_seg_lck, "tus.server");
	AN(lck_tussrv);
	tus_file_init();
	return (tus_chksum_init(ctx));
}

static int
tus_fini(VRT_CTX, struct vmod_priv *priv)
{
	(void) priv;

	AN(refcnt--);
	if (refcnt > 0)
		return (0);

	Lck_DestroyClass(&vsc_seg_lck);
	return (tus_chksum_fini(ctx));
}

//lint -sem(tus_event, thread_protected) varnish-cache ensures
int
tus_event(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	switch (e) {
	case VCL_EVENT_LOAD:    return (tus_init(ctx, priv));
	case VCL_EVENT_DISCARD: return (tus_fini(ctx, priv));
	case VCL_EVENT_WARM:
	case VCL_EVENT_COLD:
		break;
	default: INCOMPL();
	}
	return (0);
}

/* ============================================================
 * constructor/destructor
 */

/*
struct VARGS(server__init) {
	char			valid_basedir;
	VCL_STRING		basedir;
};
*/

#ifndef O_DIRECTORY
#define O_DIRECTORY 0
#endif

static int
tus_dir_chk(const char *dir)
{
	struct stat st;
	int i;

	i = stat(dir, &st);
	if (i)
		return (i);

	if (! S_ISDIR(st.st_mode)) {
		errno = ENOTDIR;
		return (-1);
	}

	i = access(dir, R_OK | W_OK);
	if (i)
		return (i);

	return (0);
}

static int
tus_basefd(const char *dir)
{
	if (tus_dir_chk(dir)) {
		if (mkdir(dir, 0700))
			return (-1);
		if (tus_dir_chk(dir))
			return (-1);
	}

	return (open(dir, O_DIRECTORY));
}

static int
tus_schemeauth_valid(const char *schemeauth, const char **p)
{
	*p = schemeauth;
	if (*p == NULL)
		return (0);
	if (! TOK(*p, "http"))
		return (0);
	if (**p == 's')
		(*p)++;
	if (! TOK(*p, "://"))
		return (0);
	if (**p == '\0')
		return (0);
	*p = strchr(*p, '/');
	if (*p)
		return (0);
	return (1);
}

static struct VPFX(tus_server) *
tus_server_new(VRT_CTX, const char *vcl_name,
    const struct VARGS(server__init) *args)
{
	struct VPFX(tus_server) *tussrv;
	const char *basedir, *p;
	char buf[PATH_MAX];
	int basefd;

	if (! tus_schemeauth_valid(args->schemeauth, &p)) {
		VRT_fail(ctx, "tus server %s: invalid schemeauth %s "
		    "at ...>%.4s<...", vcl_name, args->schemeauth, p);
		return (NULL);
	}

	basedir = args->valid_basedir ? args->basedir : vcl_name;

	basefd = tus_basefd(basedir);
	if (basefd < 0) {
		VRT_fail(ctx,
		    "tus server %s: error %d (%s) opening basedir",
		    vcl_name, errno, strerror(errno));
		return (NULL);
	}

	basedir = realpath(basedir, buf);
	if (basedir == NULL) {
		VRT_fail(ctx,
		    "tus server %s: error %d (%s) resolving basedir",
		    vcl_name, errno, strerror(errno));
		return (NULL);
	}

	ALLOC_OBJ(tussrv, VMOD_TUS_SERVER_MAGIC);
	AN(tussrv);

	Lck_New(&tussrv->mtx, lck_tussrv);
	REPLACE(tussrv->vcl_name, vcl_name);
	REPLACE(tussrv->schemeauth, args->schemeauth);
	REPLACE(tussrv->basedir, basedir);

	tussrv->basefd = basefd;
	tussrv->refcnt = 1;
	tussrv->exp = tus_file_exp_new();

	AZ(VSPLAY_INSERT(tus_servers, tus_servers, tussrv));
	return (tussrv);
}

static struct VPFX(tus_server) *
tus_server_ref(VRT_CTX, struct VPFX(tus_server) *tussrv,
    const struct VARGS(server__init) *args)
{
	const char *basedir = tussrv->basedir;

	AN(basedir);
	if (! args->valid_basedir || strcmp(basedir, args->basedir) == 0) {
		tussrv->refcnt++;
		return (tussrv);
	}

	VRT_fail(ctx,
	    "tus server %s: attempt to change basedir from %s to %s",
	    tussrv->vcl_name, basedir, args->basedir);

	return (NULL);
}

//lint -sem(tus_server__init, thread_protected) varnish-cache ensures
VCL_VOID
tus_server__init(VRT_CTX, struct VPFX(tus_server) **tussrvp,
    const char *vcl_name, struct VARGS(server__init) *args)
{
	struct vmod_blobdigest_digest *d = NULL;
	struct VPFX(tus_server) *tussrv, needle;

	AN(tussrvp);
	AZ(*tussrvp);

	if (args->valid_name_hash) {
		d = tus_hash(args->name_hash, (size_t)0);
		if (d == NULL) {
			VRT_fail(ctx, "new %s: "
			    "name_hash %s not supported "
			    "(see \"Hashes\" in documentation)",
			    vcl_name, args->name_hash);
			return;
		}
	}

	INIT_OBJ(&needle, VMOD_TUS_SERVER_MAGIC);
	needle.vcl_name = TRUST_ME(vcl_name);

	tussrv = VSPLAY_FIND(tus_servers, tus_servers, &needle);
	if (tussrv == NULL)
		tussrv = tus_server_new(ctx, vcl_name, args);
	else
		tussrv = tus_server_ref(ctx, tussrv, args);

	if (tussrv == NULL)
		return;

	tussrv->max = args->max;
	tussrv->multipart = args->multipart;
	tussrv->expires = args->expires;
	tussrv->digest = d;

	Lck_Lock(&tussrv->mtx);
	tus_file_load(tussrv);
	Lck_Unlock(&tussrv->mtx);

	*tussrvp = tussrv;

	return;
}

//lint -sem(tus_server__fini, thread_protected) varnish-cache ensures
VCL_VOID
tus_server__fini(struct VPFX(tus_server) **tussrvp)
{
	struct VPFX(tus_server) *tussrv, *remove;

	TAKE_OBJ_NOTNULL(tussrv, tussrvp, VMOD_TUS_SERVER_MAGIC);

	assert(tussrv->refcnt >= 1);
	if (--tussrv->refcnt == 0) {
		tus_file_exp_stop(tussrv->exp);
		tus_file_shutdown(tussrv);
		tus_file_exp_destroy(&tussrv->exp);
		AZ(tussrv->exp);
		remove = VSPLAY_REMOVE(tus_servers, tus_servers, tussrv);
		assert (remove == tussrv);
		Lck_Delete(&tussrv->mtx);
		REPLACE(tussrv->vcl_name, NULL);
		REPLACE(tussrv->basedir, NULL);
		AZ(close(tussrv->basefd));
		FREE_OBJ(tussrv);
	}

	return;
}

/* ============================================================
 * response to carry from recv to deliver
 */

static void
tus_task_free(VRT_CTX, void *ptr)
{
	struct tus_response *r;

	(void) ctx;
	CAST_OBJ_NOTNULL(r, ptr, VMOD_TUS_RESPONSE_MAGIC);
	if (r->fcore == NULL)
		return;
	tus_file_unlock(&r->fcore);
	AZ(r->fcore);
}

static const struct vmod_priv_methods priv_task_methods[1] = {{
		.magic = VMOD_PRIV_METHODS_MAGIC,
		.type = "vmod_tus_priv_task",
		.fini = tus_task_free
}};

static struct tus_response *
tus_task_new(VRT_CTX, const struct VPFX(tus_server) *tussrv)
{
	struct tus_response *r;
	struct vmod_priv *task;

	task = VRT_priv_task(ctx, tussrv);

	if (task == NULL) {
		VRT_fail(ctx, "no priv_task");
		return (NULL);
	}

	if (task->priv) {
		VRT_fail(ctx, "A tus request can only be started once");
		return (NULL);
	}

	r = WS_Alloc(ctx->ws, (unsigned)sizeof *r);
	if (r == NULL) {
		VRT_fail(ctx, "WS_Alloc failed");
		return (NULL);
	}
	INIT_OBJ(r, VMOD_TUS_RESPONSE_MAGIC);
	task->priv = r;
	task->methods = priv_task_methods;
	return (r);
}

static struct tus_response *
tus_task_use(VRT_CTX, const struct VPFX(tus_server) *tussrv)
{
	struct tus_response *r;
	struct vmod_priv *task;

	task = VRT_priv_task(ctx, tussrv);

	if (task == NULL) {
		VRT_fail(ctx, "no priv_task");
		return (NULL);
	}

	if (task->priv == NULL) {
		VRT_fail(ctx, "No tus request present");
		return (NULL);
	}

	CAST_OBJ_NOTNULL(r, task->priv, VMOD_TUS_RESPONSE_MAGIC);
	return (r);
}

/* ============================================================
 * tus ops
 */

/* ============================================================
 * recv / deliver
 */

/*
struct VARGS(server_recv) {
	char			valid_url;
	VCL_STRING		url;
};
*/
VCL_BOOL
tus_server_recv(VRT_CTX, struct VPFX(tus_server) *tussrv,
    struct VARGS(server_recv) *args)
{
	struct tus_response *r;
	VCL_STRING	url;
	VCL_STRING	id = NULL;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(tussrv, VMOD_TUS_SERVER_MAGIC);

	assert(ctx->method == VCL_MET_RECV);

	url = args->valid_url ? args->url : VRT_r_req_url(ctx);

	if (*url != '/') {
		VRT_fail(ctx, "%s.recv() invalid url", tussrv->vcl_name);
		return (0);
	}

	if (args->valid_id && args->id != NULL && *args->id != '\0')
		id = args->id;

	r = tus_task_new(ctx, tussrv);
	if (r == NULL)
		return (0);

	return (tus_request(ctx, tussrv, r, url, id));
}

VCL_BOOL
tus_server_deliver(VRT_CTX, struct VPFX(tus_server) *tussrv)
{
	struct tus_response *r;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(tussrv, VMOD_TUS_SERVER_MAGIC);

	assert(ctx->method == VCL_MET_DELIVER);

	r = tus_task_use(ctx, tussrv);
	if (r == NULL)
		return (0);

	tus_response(ctx, tussrv, r);
	return (0);
}

VCL_BOOL
tus_server_synth(VRT_CTX, struct VPFX(tus_server) *tussrv)
{
	struct tus_response *r;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(tussrv, VMOD_TUS_SERVER_MAGIC);

	assert(ctx->method == VCL_MET_SYNTH);

	r = tus_task_use(ctx, tussrv);
	if (r == NULL)
		return (0);

	tus_response(ctx, tussrv, r);
	return (0);
}

VCL_BOOL tus_server_done(VRT_CTX, struct VPFX(tus_server) *tussrv,
    struct VARGS(server_done)*args)
{
	struct tus_response *r;
	const char * url = NULL;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(tussrv, VMOD_TUS_SERVER_MAGIC);

	AN(ctx->method & VCL_MET_TASK_C);

	r = tus_task_use(ctx, tussrv);
	if (r == NULL)
		return (0);

	if (args->valid_location)
		url = args->location;

	return (tus_done(ctx, r, url));
}

VCL_BOOL
tus_server_has_metadata(VRT_CTX, struct VPFX(tus_server) *tussrv,
    VCL_STRING key)
{
	struct tus_response *r;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(tussrv, VMOD_TUS_SERVER_MAGIC);

	AN(ctx->method & VCL_MET_TASK_C);

	r = tus_task_use(ctx, tussrv);
	if (r == NULL)
		return (0);

	return (tus_meta(ctx, r, key, NULL));

}

VCL_BLOB
tus_server_metadata(VRT_CTX, struct VPFX(tus_server) *tussrv,
    VCL_STRING key)
{
	struct tus_response *r;
	VCL_BLOB b;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(tussrv, VMOD_TUS_SERVER_MAGIC);

	AN(ctx->method & VCL_MET_TASK_C);

	r = tus_task_use(ctx, tussrv);
	if (r == NULL)
		return (NULL);

	if (tus_meta(ctx, r, key, &b) == 0)
		return (NULL);

	return (b);
}

VCL_VOID
tus_server_sync(VRT_CTX, struct VPFX(tus_server) *tussrv)
{
	struct tus_response *r;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(tussrv, VMOD_TUS_SERVER_MAGIC);

	r = tus_task_use(ctx, tussrv);
	if (r == NULL)
		return;

	tus_file_sync(r->fcore);
}
