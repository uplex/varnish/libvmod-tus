/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

struct tus_status {
	unsigned	status;
	const char	*reason;
};

struct tus_response {
	unsigned			magic;
#define VMOD_TUS_RESPONSE_MAGIC	0x1054e570
	struct tus_status		s;
	const char			*schemeauth;	// from tussrv
	const char			*origin;	// CORS
	// if set, mtx is helt
	struct tus_file_core		*fcore;
};

void
tus_response(VRT_CTX, const struct VPFX(tus_server) *tussrv,
    const struct tus_response *resp);
VCL_BOOL
tus_done(VRT_CTX, const struct tus_response *resp, const char *url);
VCL_BOOL
tus_meta(VRT_CTX, const struct tus_response *resp, const char *k, VCL_BLOB *b);

extern const unsigned s_MULT;
extern const unsigned s_OPTIONS;
