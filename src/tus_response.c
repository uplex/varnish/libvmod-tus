/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <sys/statvfs.h>
#include <stdio.h>

#include <cache/cache.h>
#include <vrt_obj.h>

#include "tus_checksums.h"
#include "tus_file.h"
#include "tus_hdr.h"
#include "tus_response.h"

const unsigned s_MULT = 1000;
const unsigned s_OPTIONS = 1;

static const char * const allow = "POST, GET, HEAD, PATCH, DELETE, OPTIONS";

// vmod/vmod_std_conversions.c
#define VCL_BYTES_MAX ((INT64_C(1)<<53)-1)

static VCL_BYTES
tus_upload_length(const struct VPFX(tus_server) *tussrv,
    const struct tus_file_core *fcore)
{
	VCL_BYTES srvmax;
	struct statvfs fs;
	size_t spc;

	srvmax = tus_server_max(tussrv);
	assert(srvmax >= 0);
	assert(srvmax <= VCL_BYTES_MAX);

	if (fcore != NULL && fcore->fd >= 0) {
		if (fstatvfs(fcore->fd, &fs))
			return (srvmax);
	} else if (statvfs(tus_server_basedir(tussrv), &fs))
		return (srvmax);

	spc = fs.f_bavail * fs.f_frsize;
	if (spc < (size_t)srvmax)
		return ((VCL_BYTES)spc);
	return (srvmax);
}

static void
tus_cors(VCL_HTTP r, unsigned status, const char *origin)
{
	http_ForceHeader(r, hdr_acao, origin);

	// from tus go server pkg/handler/unrouted_handler.go
	if (status / s_MULT == s_OPTIONS) {
		http_ForceHeader(r, hdr_acam, allow);
		http_ForceHeader(r, hdr_acah,
		    "Authorization, Origin, X-Requested-With, X-Request-ID, "
		    "X-HTTP-Method-Override, Content-Type, Upload-Length, "
		    "Upload-Offset, Tus-Resumable, Upload-Metadata, "
		    "Upload-Defer-Length, Upload-Concat");
		http_ForceHeader(r, hdr_acma, "86400");
	} else {
		http_ForceHeader(r, hdr_aceh,
		    "Upload-Offset, Content-Location, Location, Upload-Length, Tus-Version, "
		    "Tus-Resumable, Tus-Max-Size, Tus-Extension, "
		    "Upload-Metadata, Upload-Defer-Length, Upload-Concat");
	}
}

void
tus_response(VRT_CTX, const struct VPFX(tus_server) *tussrv,
    const struct tus_response *resp)
{
	const struct tus_file_disk *fdisk = NULL;
	const struct tus_file_core *fcore = NULL;
	const char *chksums = tus_checksums();
	VCL_HTTP r;
	char t[VTIM_FORMAT_SIZE];
	const char *loc;
	char buf[TUS_PATH_MAX];

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(resp, VMOD_TUS_RESPONSE_MAGIC);
	if (resp->fcore) {
		fcore = resp->fcore;
		fdisk = fcore->disk;
		AN(fdisk);
	}
	CHECK_OBJ_ORNULL(fdisk, VMOD_TUS_FILE_DISK_MAGIC);

	r = ctx->http_resp;
	CHECK_OBJ_NOTNULL(r, HTTP_MAGIC);

	http_Unset(r, hdr_max);
	http_Unset(r, hdr_off);
	http_Unset(r, hdr_len);
	http_Unset(r, hdr_defer);
	http_Unset(r, hdr_meta);
	http_Unset(r, hdr_exp);
	http_Unset(r, hdr_loc);
	http_Unset(r, hdr_cloc);

	if (fdisk != NULL && fdisk->location_length > 0) {
		if (fdisk->location[0] == '/' && fdisk->location[1] != '/') {
			bprintf(buf, "%s%s", resp->schemeauth, fdisk->location);
			loc = buf;
		} else {
			loc = fdisk->location;
		}
		http_ForceHeader(r,
		    resp->s.status == 301 ? hdr_loc : hdr_cloc,
		    loc);
	}

	VRT_l_resp_status(ctx, (VCL_INT)resp->s.status);
	if (resp->s.status == 301)
		return;

	if (resp->s.reason != NULL) {
		VRT_l_resp_reason(ctx, resp->s.reason, vrt_null_strands);
		// FCK H2 - stupid idea to eliminate the reason
		http_ForceHeader(r, hdr_reason, resp->s.reason);
	}
	if (resp->s.status == 405) {
		http_ForceHeader(r, hdr_allow, allow);
		return;
	}

	http_ForceHeader(r, hdr_resum, "1.0.0");
	http_ForceHeader(r, hdr_vers, "1.0.0");
	if (chksums != NULL) {
		http_ForceHeader(r, hdr_ext, "creation,creation-with-upload,"
		    "expiration,termination,concatenation,checksum");
		http_ForceHeader(r, hdr_chkalg, chksums);
	} else {
		http_ForceHeader(r, hdr_ext, "creation,creation-with-upload,"
		    "expiration,termination,concatenation");
	}

	if (resp->origin != NULL && *resp->origin != '\0')
		tus_cors(r, resp->s.status, resp->origin);

	http_PrintfHeader(r, "Tus-Max-Size: %ju",
	    tus_upload_length(tussrv, fcore));

	while (fdisk) {
		AN(fcore);

		if (fdisk->upload_offset >= 0)
			http_PrintfHeader(r, "Upload-Offset: %zi",
			    fdisk->upload_offset);
		if (fdisk->upload_length >= 0)
			http_PrintfHeader(r, "Upload-Length: %zi",
			    fdisk->upload_length);
		else if (fdisk->upload_length == -1)
			http_ForceHeader(r, hdr_defer, "1");
		else
			INCOMPL();
		if (fdisk->metadata_length != 0)
			http_ForceHeader(r, hdr_meta, fdisk->metadata);
		VTIM_format(fdisk->upload_expires, t);
		http_ForceHeader(r, hdr_exp, t);
		if (resp->s.status == 201) {
			AN(resp->schemeauth);
			http_PrintfHeader(r, "Location: %s%s", resp->schemeauth,
			    fdisk->upload_path);
		}

		switch (fdisk->type) {
		case TUS_SINGLE:
			break;
		case TUS_FINAL:
			if (fcore->ptr == NULL)
				break;
			AN(resp->schemeauth);
			http_ForceHeader(r, hdr_concat,
			    tus_file_final_urls(ctx,
			    resp->fcore, resp->schemeauth));
			break;
		case TUS_PARTIAL:
			http_ForceHeader(r, hdr_concat, "partial");
			break;
		case _TUS_TYPE_LIMIT:
		default:
			INCOMPL();
		}
		break;
	}
}

VCL_BOOL
tus_done(VRT_CTX, const struct tus_response *resp, const char *url)
{
	struct tus_file_core *fcore;
	struct tus_file_disk *fdisk;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(resp, VMOD_TUS_RESPONSE_MAGIC);
	fcore = resp->fcore;
	if (fcore == NULL)
		return (0);
	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	fdisk = fcore->disk;
	CHECK_OBJ_NOTNULL(fdisk, VMOD_TUS_FILE_DISK_MAGIC);

	if (fdisk->type != TUS_SINGLE && fdisk->type != TUS_FINAL)
		return (0);

	return (tus_file_done(fcore, fdisk, url));
}

VCL_BOOL
tus_meta(VRT_CTX, const struct tus_response *resp, const char *k, VCL_BLOB *b)
{
	const struct tus_file_core *fcore;
	const struct tus_file_disk *fdisk;

	CHECK_OBJ_NOTNULL(resp, VMOD_TUS_RESPONSE_MAGIC);

	fcore = resp->fcore;
	if (fcore == NULL)
		return (0);
	CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
	fdisk = fcore->disk;
	if (fdisk == NULL)
		return (0);
	CHECK_OBJ_NOTNULL(fdisk, VMOD_TUS_FILE_DISK_MAGIC);

	return (tus_file_meta(ctx, fdisk, k, b));
}
