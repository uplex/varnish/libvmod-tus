/*-
 * Copyright 2020-2023 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cache/cache.h"
#include "vsb.h"

#include "import_vmod_blob.h"
#include "import_vmod_blobdigest.h"
#include "tus_b64.h"
#include "tus_blob.h"
#include "tus_checksums.h"

//avoid defined but not used
static inline void
avoid_compiler_warnings(void) {
	(void)vmod_priv_blobdigest;
	(void)VGC_vmod_blobdigest;
	(void)vmod_priv_blob;
	(void)VGC_vmod_blob;
}

static const struct Vmod_vmod_blob_Func *vmod_blob = NULL;
//lint -esym(459, vmod_blobdigest) unprotected access
static const struct Vmod_vmod_blobdigest_Func *vmod_blobdigest = NULL;
static void *dl_vmod_blob = NULL;
static void *dl_vmod_blobdigest = NULL;

/* ------------------------------------------------------------
 * linkage to blob and blobdigest vmods
 */

static void
load_vmod(const struct vmod_data *data, void **hdl, const void **func)
{
	const struct vmod_data *d;
	void *dlhdl;
	char buf[256];

	bprintf(buf, "./vmod_cache/_vmod_%s.%s",
	    data->name, data->file_id);

	dlhdl = dlopen(buf, RTLD_LAZY | RTLD_LOCAL);
	if (dlhdl == NULL) {
		fprintf(stderr, "dlopen vmod_%s: %s\n", data->name, dlerror());
		return;
	}
	bprintf(buf, "Vmod_%s_Data", data->name);
	d = dlsym(dlhdl, buf);
	if (d == NULL) {
		fprintf(stderr, "dlsym vmod_%s: %s\n", data->name, dlerror());
		(void) dlclose(dlhdl);
		return;
	}
	if (strcmp(d->name, data->name) ||
	    strcmp(d->file_id, data->file_id) ||
	    strcmp(d->abi, data->abi)) {
		fprintf(stderr, "vmod_%s: name/id/abi mismatch\n", data->name);
		(void) dlclose(dlhdl);
		return;
	}
	*hdl = dlhdl;
	*func = d->func;
	fprintf(stderr, "vmod_%s OK\n", data->name);
}

#define HENUM(t, b) static struct vmod_blobdigest_digest *digest_ ## t = NULL;
#include "tbl_hash_enum.h"

//lint -esym(459, enabled) unprotected access
static unsigned enabled = 0;
static char *hashes = NULL;

int
tus_chksum_init(VRT_CTX) {
	struct vsb names[1];
	char buf[512];
	int first = 1;

	AN(VSB_init(names, buf, sizeof buf));

	load_vmod(&import_Vmod_blob_Data, &dl_vmod_blob,
	    (const void **)&vmod_blob);
	load_vmod(&import_Vmod_blobdigest_Data, &dl_vmod_blobdigest,
	    (const void **)&vmod_blobdigest);

	if (vmod_blobdigest != NULL && vmod_blob != NULL)
		enabled = 1;
	else
		return (0);

	fprintf(stderr, "enabled %u\n", enabled);

#define HENUM(t, b) do {						\
		vmod_blobdigest->f_digest__init(ctx, &digest_ ## t,	\
		    "tus_digest_" #t, *vmod_blobdigest->enum_ ## b, NULL, \
		    *vmod_blobdigest->enum_TASK);			\
		if (digest_ ## t == NULL)				\
			break;						\
		if (first == 0)						\
			AZ(VSB_cat(names, ","));			\
		AZ(VSB_cat(names, #t));					\
		first = 0;						\
	} while (0);
#include "tbl_hash_enum.h"
	AZ(VSB_finish(names));
	REPLACE(hashes, VSB_data(names));
	VSB_fini(names);
	fprintf(stderr, "digests OK\n");

	// dummy ref to appease compiler
	memset(&Vmod_vmod_blobdigest_Func, 0, sizeof Vmod_vmod_blobdigest_Func);
	memset(&Vmod_vmod_blob_Func, 0, sizeof Vmod_vmod_blob_Func);
	return (0);
}

int
tus_chksum_fini(VRT_CTX) {

	(void) ctx;
#define HENUM(t, b) if (digest_ ## t != NULL)			\
		vmod_blobdigest->f_digest__fini(&digest_ ## t);
#include "tbl_hash_enum.h"
	REPLACE(hashes, NULL);//lint !e539
	if (dl_vmod_blob != NULL)
		(void) dlclose(dl_vmod_blob);
	if (dl_vmod_blobdigest != NULL)
		(void) dlclose(dl_vmod_blobdigest);
	return (0);
}

/* ------------------------------------------------------------
 * wrap base64
 */
VCL_BLOB
tus_b64_decode(VRT_CTX, const char *s, VCL_INT l)
{
	struct strands st = {.n = 1, .p = &s};

	if (! enabled) {
		VRT_fail(ctx, "base64 decode support not available - "
			 "vmod_blob was not found");
		return (NULL);
	}

	AN(vmod_blob);
	AN(vmod_blob->f_decode);
	AN(vmod_blob->enum_BASE64);

	return (vmod_blob->f_decode(ctx, *vmod_blob->enum_BASE64, l, &st));
}

/* ------------------------------------------------------------
 * enabled checksums
 */
const char *
tus_checksums(void)
{
	return (enabled ? hashes : NULL);
}

/* ------------------------------------------------------------
 * handling of tus checksums
 */

struct tus_chksum {
	unsigned			magic;
#define VMOD_TUS_CHKSUM_MAGIC		0x105c6650
	struct vmod_blobdigest_digest	*digest;
	VCL_BLOB			expect;
	VCL_BLOB			final;
};

/* tus hash names to blobdiget object */
struct vmod_blobdigest_digest *
tus_hash(const char *s, size_t l)
{
	if (enabled == 0 || s == NULL)
		return (NULL);

	if (l == 0)
		l = strlen(s);

#define HENUM(t, b)							\
	if (strncmp(s, #t, l) == 0) return (digest_ ## t);
#include "tbl_hash_enum.h"
	return (NULL);
}

struct tus_chksum *
tus_chksum_new(VRT_CTX, struct vmod_blobdigest_digest *d)
{
	struct tus_chksum *r;

	AN(d);

	r = WS_Alloc(ctx->ws, (unsigned)sizeof *r);
	if (r == NULL) {
		VRT_fail(ctx, "WS_Alloc failed");
		return (NULL);
	}
	INIT_OBJ(r, VMOD_TUS_CHKSUM_MAGIC);
	r->digest = d;

	return (r);
}

struct tus_chksum *
tus_chksum_hdr(VRT_CTX, const char *hdr)
{
	struct vmod_blobdigest_digest *d;
	struct tus_chksum *r;
	VCL_BLOB expect;
	const char *sep;

	if (enabled == 0 || hdr == NULL)
		return (NULL);

	sep = strchr(hdr, ' ');
	if (sep == NULL)
		return (NULL);

	assert(sep >= hdr);
	d = tus_hash(hdr, (size_t)(sep - hdr));
	if (d == NULL)
		return (NULL);

	r = tus_chksum_new(ctx, d);
	if (r == NULL)
		return (NULL);

	sep++;

	expect = tus_b64_decode(ctx, sep, (size_t)0);

	if (expect == NULL)
		return (NULL);

	r->expect = expect;
	return (r);
}

void
tus_chksum_update(VRT_CTX, const struct tus_chksum *c,
    const void *ptr, size_t l)
{
	struct vrt_blob b;
	struct arg_vmod_blobdigest_digest_update a = {0};

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(c, VMOD_TUS_CHKSUM_MAGIC);
	AN(enabled);

	b.type = 0x105;
	b.len = l;
	b.blob = ptr;

	a.valid_msg = 1;
	a.msg = &b;

	(void) vmod_blobdigest->f_digest_update(ctx, c->digest, &a);
}

VCL_BLOB
tus_chksum_final(VRT_CTX, struct tus_chksum *c)
{

	AN(enabled);
	if (c->final == NULL)
		c->final = vmod_blobdigest->f_digest_final(ctx, c->digest);
	return (c->final);
}

VCL_BOOL
tus_chksum_equal(VRT_CTX, struct tus_chksum *c)
{

	AN(vmod_blob);
	AN(vmod_blob->f_equal);
	(void) tus_chksum_final(ctx, c);
	return (vmod_blob->f_equal(ctx, c->expect, c->final));
}
