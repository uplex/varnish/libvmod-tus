/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include "cache/cache.h"
#include "vsb.h"

#include "tus_blob.h"
#include "tus_checksums.h"

int
tus_chksum_init(VRT_CTX) {
	(void) ctx;
	return (0);
}

int
tus_chksum_fini(VRT_CTX) {
	(void) ctx;
	return (0);
}

/* ------------------------------------------------------------
 * wrap base64
 */
VCL_BLOB
tus_b64_decode(VRT_CTX, const char *s, VCL_INT l)
{
	(void) s;
	(void) l;

	VRT_fail(ctx, "base64 decode support not available - "
		 "vmod_blob support not compiled in");

	return (NULL);
}

/* ------------------------------------------------------------
 * enabled checksums
 */
const char *
tus_checksums(void)
{
	return (NULL);
}

/* ------------------------------------------------------------
 * handling of tus checksums
 */

/* tus hash names to blobdiget object */
struct vmod_blobdigest_digest *
tus_hash(const char *s, size_t l)
{
	(void) s;
	(void) l;

	return (NULL);
}

struct tus_chksum *
tus_chksum_new(VRT_CTX, struct vmod_blobdigest_digest *d)
{
	(void) ctx;
	(void) d;

	return (NULL);
}

struct tus_chksum *
tus_chksum_hdr(VRT_CTX, const char *hdr)
{
	(void) ctx;
	(void) hdr;

	return (NULL);
}

void
tus_chksum_update(VRT_CTX, const struct tus_chksum *c,
    const void *ptr, size_t l)
{
	(void) ctx;
	(void) c;
	(void) ptr;
	(void) l;
	INCOMPL();
}

VCL_BLOB
tus_chksum_final(VRT_CTX, struct tus_chksum *c)
{
	(void) ctx;
	(void) c;
	INCOMPL();
	return (NULL);
}

VCL_BOOL
tus_chksum_equal(VRT_CTX, struct tus_chksum *c)
{
	(void) tus_chksum_final(ctx, c);
	return (0);
}
