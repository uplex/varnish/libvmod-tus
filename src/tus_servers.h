/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <string.h>
#include <stdlib.h>

#include <vtree.h>

/* ============================================================
 * global server splay tree
 */
struct VPFX(tus_server) {
	unsigned				magic;
#define VMOD_TUS_SERVER_MAGIC			0x1055e47e
	unsigned				refcnt;

	VSPLAY_ENTRY(VPFX(tus_server))		entry;

	struct lock				mtx;
	char					*vcl_name;
	char					*basedir;
	char					*schemeauth;
	VCL_BYTES				max;
	VCL_BYTES				multipart;
	VCL_DURATION				expires;
	int					basefd;
	struct tus_files			files[1];
	struct vmod_blobdigest_digest		*digest;
	struct tus_exp				*exp;
};

static inline int
tus_server_cmp(const struct VPFX(tus_server) *a,
    const struct VPFX(tus_server) *b) {
	CHECK_OBJ_NOTNULL(a, VMOD_TUS_SERVER_MAGIC);
	CHECK_OBJ_NOTNULL(b, VMOD_TUS_SERVER_MAGIC);
	return (strcmp(a->vcl_name, b->vcl_name));
}
VSPLAY_HEAD(tus_servers, VPFX(tus_server));
extern struct tus_servers tus_servers[1];
VSPLAY_PROTOTYPE(tus_servers, VPFX(tus_server), entry, tus_server_cmp)
