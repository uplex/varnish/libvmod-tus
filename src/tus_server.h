/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

// pseudo-opaque declaration
#ifndef VPFX
#define VPFX(x) tus_ ## x
#endif
struct VPFX(tus_server);

struct tus_exp;
struct vmod_blobdigest_digest;

// tus_servers.c
const char * tus_server_name(const struct VPFX(tus_server) *);
struct tus_files *tus_server_files(struct VPFX(tus_server) *);
int tus_server_basefd(const struct VPFX(tus_server) *);
const char * tus_server_basedir(const struct VPFX(tus_server) *);
VCL_DURATION tus_server_expires(const struct VPFX(tus_server) *);
void tus_server_lock(struct VPFX(tus_server) *);
void tus_server_unlock(struct VPFX(tus_server) *);
void tus_server__assertlocked(struct VPFX(tus_server) *s, const char *func,
    const char *file, int line);
#define tus_server_AssertLocked(s)					\
	tus_server__assertlocked(s, __func__, __FILE__, __LINE__)
VCL_BYTES tus_server_max(const struct VPFX(tus_server) *s);
VCL_BYTES tus_server_multipart(const struct VPFX(tus_server) *s);
struct vmod_blobdigest_digest * tus_server_digest(
    const struct VPFX(tus_server) *s);
struct tus_exp *tus_server_exp(const struct VPFX(tus_server) *);
const char * tus_server_schemeauth(const struct VPFX(tus_server) *s);
