/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <sys/mman.h>

#include <cache/cache.h>

#include <cache/cache_obj.h>
#include <cache/cache_objhead.h>
#include <storage/storage.h>

#include "tus_file.h"
#include "tus_concat.h"
#include "tus_stv.h"

/* from varnish-cache vend.h */
static __inline void
vbe32enc(void *pp, uint32_t u)
{
	uint8_t *p = (uint8_t *)pp;

	p[0] = (u >> 24) & 0xff;
	p[1] = (u >> 16) & 0xff;
	p[2] = (u >> 8) & 0xff;
	p[3] = u & 0xff;
}

static __inline void
vbe64enc(void *pp, uint64_t u)
{
	uint8_t *p = (uint8_t *)pp;

	vbe32enc(p, (uint32_t)(u >> 32));
	vbe32enc(p + 4, (uint32_t)(u & 0xffffffffU));
}


static void
tus_objfree(struct worker *wrk, struct objcore *oc);
static int
tus_objiterator(struct worker *wrk, struct objcore *oc,
void *priv, objiterate_f *func, int final);
static int
tus_objgetspace(struct worker *wrk, struct objcore *oc,
     ssize_t *sz, uint8_t **ptr);
static void
tus_objextend(struct worker *wrk, struct objcore *oc, ssize_t l);
static const void *
tus_objgetattr(struct worker *wrk, struct objcore *oc,
enum obj_attr attr, ssize_t *len);
static void *
tus_objsetattr(struct worker *wrk, struct objcore *oc,
enum obj_attr attr, ssize_t len, const void *ptr);

static const struct obj_methods obj_tus = {
	/* required */
	.objfree	= tus_objfree,
	.objiterator	= tus_objiterator,
	.objgetspace	= tus_objgetspace,
	.objextend	= tus_objextend,
	.objgetattr	= tus_objgetattr,
	.objsetattr	= tus_objsetattr,
	/* optional */
	.objtrimstore	= NULL,
	.objbocdone	= NULL,
	.objslim	= NULL,
	.objtouch	= NULL,
	.objsetstate	= NULL
};
// ------------------------------------------------------------

static int
tus_allocobj(struct worker *wrk, const struct stevedore *stv,
    struct objcore *oc, unsigned l)
{
	(void) wrk;
	(void) stv;
	(void) oc;
	(void) l;
	INCOMPL();
	return (0);
}

static const struct stevedore stv_tus = {
	.magic = STEVEDORE_MAGIC,
	.name = "tus",

	.allocobj = tus_allocobj,
	.methods = &obj_tus,

	.ident = "tus",
	.vclname = "tus"
};


/*
 * ============================================================
 *
 */

// instead of STV_NewObject()
//
// assign an existing tus file to an objcore
void
tus_body_assign(struct req *req, struct tus_concat *c)
{
	struct objcore *oc;

	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	CHECK_OBJ_NOTNULL(c, TUS_CONCAT_MAGIC);

	if (req->body_oc != NULL)
		AZ(HSH_DerefObjCore(req->wrk, &req->body_oc, 0));

	AZ(req->body_oc);
	req->body_oc = oc = HSH_Private(req->wrk);
	AN(oc);

	AZ(oc->stobj->stevedore);
	AZ(oc->stobj->priv);

	oc->oa_present = 0;
	oc->stobj->stevedore = &stv_tus;
	oc->stobj->priv = c;
	//oc->stobj->priv2 = 0;

	req->req_body_status = BS_CACHED;
	HSH_DerefBoc(req->wrk, oc);
}

static void
tus_objfree(struct worker *wrk, struct objcore *oc)
{

	(void) wrk;
	// not actually freeing it, the tus object gets expired / refcounted

	oc->stobj->stevedore = NULL;
	oc->stobj->priv = 0;
}

static int
tus_objiterator(struct worker *wrk, struct objcore *oc,
    void *priv, objiterate_f *func, int final)
{
	// sensible upper limit taking into account tcp windows
	const size_t max_write = 16 * 1024 * 1024;
	struct tus_concat *c;
	struct tus_file_core *fcore;
	unsigned flags, i;
	size_t off, len;
	void *p;
	int r = -1;

	(void) wrk;
	(void) final;
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	assert(oc->stobj->stevedore == &stv_tus);

	CAST_OBJ_NOTNULL(c, oc->stobj->priv, TUS_CONCAT_MAGIC);
	oc = NULL;
	(void) oc;	// flexelint

	flags = OBJ_ITER_FLUSH;

	for (i = 0; i < c->n; i++) {
		fcore = c->cores[i];
		CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
		if (fcore->len == 0)
			continue;
		AN(fcore->ptr);

		p = fcore->ptr;
		len = fcore->len;
		AZ(posix_madvise(p, len, POSIX_MADV_SEQUENTIAL));
		AZ(posix_madvise(p, len, POSIX_MADV_WILLNEED));
		off = 0;
		do {
			len = fcore->len - off;
			if (len > max_write)
				len = max_write;
			assert(len <= SSIZE_MAX);

			p = (char *)fcore->ptr + off;
			off += len;

			if (off == fcore->len && i == c->n)
				flags |= OBJ_ITER_END;

			r = func(priv, flags, p, (ssize_t)len);

			if (r < 0)
				return (r);

			AZ(posix_madvise(p, len, POSIX_MADV_DONTNEED));

		} while (off < fcore->len);
	}
	if (r == 0 && (flags & OBJ_ITER_END) == 0)
		r = func(priv, flags | OBJ_ITER_END, NULL, (ssize_t)0);
	return (r);
}

static int
tus_objgetspace(struct worker *wrk, struct objcore *oc,
     ssize_t *sz, uint8_t **ptr)
{
	(void) wrk;
	(void) oc;
	(void) sz;
	(void) ptr;
	INCOMPL();
	return (0);
}

static void
tus_objextend(struct worker *wrk, struct objcore *oc, ssize_t l)
{
	(void) wrk;
	(void) oc;
	(void) l;
	INCOMPL();
}

static const void *
tus_objgetattr(struct worker *wrk, struct objcore *oc,
    enum obj_attr attr, ssize_t *len)
{
	struct tus_file_core *fcore;
	struct tus_concat *c;
	unsigned i;
	uint64_t l;

	(void) wrk;
	(void) oc;

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	assert(oc->stobj->stevedore == &stv_tus);

	CAST_OBJ_NOTNULL(c, oc->stobj->priv, TUS_CONCAT_MAGIC);

	/*lint -e{788} enums not used */
	switch (attr) {
	case OA_LEN:
		l = 0;
		for (i = 0; i < c->n; i++) {
			fcore = c->cores[i];
			CHECK_OBJ_NOTNULL(fcore, VMOD_TUS_FILE_CORE_MAGIC);
			l += fcore->len;
		}
		vbe64enc(&c->len, l);
		*len = sizeof(c->len);
		return (&c->len);
	default:
		INCOMPL();
	}
	return (NULL);
}

static void *
tus_objsetattr(struct worker *wrk, struct objcore *oc,
enum obj_attr attr, ssize_t len, const void *ptr)
{
	(void) wrk;
	(void) oc;
	(void) attr;
	(void) len;
	(void) ptr;
	INCOMPL();
	return (NULL);
}

// helper

struct tus_concat *
tus_body_single(struct req *req, struct tus_file_core *fcore)
{
	struct tus_concat *c;

	c = WS_Alloc(req->ws, (unsigned)(sizeof *c + sizeof fcore));
	if (c == NULL)
		return (NULL);
	INIT_OBJ(c, TUS_CONCAT_MAGIC);
	c->n = 1;
	c->cores[0] = fcore;
	return (c);
}
