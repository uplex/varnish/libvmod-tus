/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "tus_hdr.h"

const char * const hdr_resum	= "\016Tus-Resumable:";
const char * const hdr_vers	= "\014Tus-Version:";
const char * const hdr_ext	= "\016Tus-Extension:";
const char * const hdr_chkalg	= "\027Tus-Checksum-Algorithm:";
const char * const hdr_chksum	= "\020Upload-Checksum:";
const char * const hdr_concat	= "\016Upload-Concat:";
const char * const hdr_max	= "\015Tus-Max-Size:";
const char * const hdr_defer	= "\024Upload-Defer-Length:";
const char * const hdr_meta	= "\020Upload-Metadata:";
const char * const hdr_exp	= "\017Upload-Expires:";
const char * const hdr_cloc	= "\021Content-Location:";
const char * const hdr_loc	= "\011Location:";
const char * const hdr_method	= "\027X-HTTP-Method-Override:";
const char * const hdr_off	= "\016Upload-Offset:";
const char * const hdr_len	= "\016Upload-Length:";

const char * const hdr_origin	= "\007Origin:";
const char * const hdr_acah	= "\035Access-Control-Allow-Headers:";
const char * const hdr_acam	= "\035Access-Control-Allow-Methods:";
const char * const hdr_acma	= "\027Access-Control-Max-Age:";
const char * const hdr_acao	= "\034Access-Control-Allow-Origin:";
const char * const hdr_aceh	= "\036Access-Control-Expose-Headers:";

const char * const hdr_allow	= "\006Allow:";

const char * const hdr_reason	= "\007Reason:";

const char * const hdr_vtc	= "\015VTC-Filename:";
