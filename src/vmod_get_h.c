/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "cache/cache.h"
#include "vas.h"

// provide symbols from varnish-cache used by vmods
const struct body_status BS_NONE[1];
const struct body_status BS_TAKEN[1];
const struct body_status BS_ERROR[1];

const struct vrt_blob *vrt_null_blob = &(struct vrt_blob){
	.type = VRT_NULL_BLOB_TYPE,
	.len = 0,
	.blob = ""
};

int
main(int argc, char * const *argv) {
	void *dlhdl;
	char buf[256], vcl[64];
	const struct vmod_data *d;
	const char *name;
	const char *file;
	int i, fd;
	ssize_t ssz;

	if (argc < 3) {
		fprintf(stderr, "need 2 arguments\n");
		return (1);
	}

	name = argv[1];
	file = argv[2];

	bprintf(buf, "vcl 4.1;backend none none;import %s from \"%s\";\n",
		name, file);

	bprintf(vcl, "/tmp/vmod_get_h_%s_XXXXXX", name);
	fd = mkstemp(vcl);
	assert(fd >= 0);
	ssz = write(fd, buf, strlen(buf));
	assert(ssz > 0);
	i = close(fd);
	assert(i == 0);

	bprintf(buf, "varnishd -Cf %s 2>&1 | awk '"
		"/BEGIN VMOD %s/ {p=1} "
		"/END VMOD %s/ {exit} "
		"/./ {if (p == 1) { print }; n;}"
		"'", vcl, name, name);
	printf("/* %s */\n", buf);
	i = system(buf);
	assert(i == 0);

	dlhdl = dlopen(file, RTLD_LAZY | RTLD_LOCAL);
	if (dlhdl == NULL) {
		fprintf(stderr, "dlopen: %s\n", dlerror());
		return (1);
	}
	bprintf(buf, "Vmod_%s_Data", name);
	d = dlsym(dlhdl, buf);
	if (d == NULL) {
		fprintf(stderr, "dlsym: %s\n", dlerror());
		return (1);
	}

	printf("\nstatic const struct vmod_data import_%s = {\n", buf);
	printf("\t.name =\t\t\"%s\",\n", d->name);
	printf("\t.file_id =\t\"%s\",\n", d->file_id);
	printf("\t.abi =\t\t\"%s\",\n", d->abi);
	printf("};\n");

	return (0);
}
