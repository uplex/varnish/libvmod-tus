/*lint -save -e525 -e539 */
// HENUM(tusname, blobdigestenum)
HENUM(crc32, CRC32)
HENUM(icrc32, ICRC32)
HENUM(md5, MD5)
HENUM(rs, RS)
HENUM(sha1, SHA1)
HENUM(sha224, SHA224)
HENUM(sha256, SHA256)
HENUM(sha384, SHA384)
HENUM(sha3_224, SHA3_224)
HENUM(sha3_256, SHA3_256)
HENUM(sha3_384, SHA3_384)
HENUM(sha3_512, SHA3_512)
HENUM(sha512, SHA512)
#undef HENUM
/*lint -restore */
