/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <pthread.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>

#include "cache/cache.h"	// for struct lock
#include "miniobj.h"
#include "vas.h"
#include "vbh.h"

#include "tus_file.h"
#include "tus_file_exp.h"
#include "tus_file_exp_setup.h"

struct tus_exp {
	unsigned	magic;
#define TUS_EXP_MAGIC	0x105e8900
	unsigned	die;
	struct vbh	*heap;
	pthread_mutex_t	mtx;
	pthread_cond_t	cond;
	pthread_t	thread;
};

static inline vtim_real
fcore_when(const void *p)
{
	const struct tus_file_core *fcore;
	const struct tus_file_disk *fdisk;

	CAST_OBJ_NOTNULL(fcore, p, VMOD_TUS_FILE_CORE_MAGIC);
	fdisk = fcore->disk;
	CHECK_OBJ_NOTNULL(fdisk, VMOD_TUS_FILE_DISK_MAGIC);
	return (fdisk->upload_expires);
}

static void *
tus_exp_thread(void *p)
{
	struct tus_file_core *fcore;
	struct timespec ts;
	struct tus_exp *e;
	vtim_real now, when, t;

	CAST_OBJ_NOTNULL(e, p, TUS_EXP_MAGIC);
	AZ(pthread_mutex_lock(&e->mtx));
	while (! e->die) {
		fcore = VBH_root(e->heap);
		if (fcore == NULL) {
			AZ(pthread_cond_wait(&e->cond, &e->mtx));
			continue;
		}

		when = fcore_when(fcore);
		now = VTIM_real();
		if (when > now) {
			assert(when > 1e9);
			ts.tv_nsec = (long)(modf(when, &t) * 1e9);
			ts.tv_sec = (long)t;
			assert(ts.tv_nsec >= 0 && ts.tv_nsec < 999999999);
			errno = pthread_cond_timedwait(&e->cond, &e->mtx, &ts);
			assert(errno == 0 || errno == ETIMEDOUT ||
			    errno == EINTR);
			continue;
		}

		assert(fcore->exp_idx != VBH_NOIDX);
		VBH_delete(e->heap, fcore->exp_idx);

		AZ(pthread_mutex_unlock(&e->mtx));
		AN(fcore);
		tus_file_exp(&fcore);
		AZ(fcore);
		AZ(pthread_mutex_lock(&e->mtx));
	}
	AZ(pthread_mutex_unlock(&e->mtx));
	return (NULL);
}

/* only during shutdown, exp is already stopped */
void
tus_exp_delete(const struct tus_file_core *fcore)
{
	struct tus_exp *e = tus_server_exp(fcore->server);

	CHECK_OBJ_NOTNULL(e, TUS_EXP_MAGIC);

	assert(e->die == 1);
	assert(e->thread == 0);
	assert(fcore->exp_idx != VBH_NOIDX);
	VBH_delete(e->heap, fcore->exp_idx);
}

void
tus_exp_insert(struct tus_file_core *fcore)
{
	struct tus_exp *e = tus_server_exp(fcore->server);

	CHECK_OBJ_NOTNULL(e, TUS_EXP_MAGIC);

	AZ(pthread_mutex_lock(&e->mtx));
	VBH_insert(e->heap, fcore);
	if (VBH_root(e->heap) == fcore)
		AZ(pthread_cond_signal(&e->cond));
	AZ(pthread_mutex_unlock(&e->mtx));
}

void
tus_exp_touch(const struct tus_file_core *fcore)
{
	struct tus_exp *e = tus_server_exp(fcore->server);

	CHECK_OBJ_NOTNULL(e, TUS_EXP_MAGIC);

	AZ(pthread_mutex_lock(&e->mtx));
	VBH_reorder(e->heap, fcore->exp_idx);
	if (VBH_root(e->heap) == fcore)
		AZ(pthread_cond_signal(&e->cond));
	AZ(pthread_mutex_unlock(&e->mtx));
}

static int
tus_exp_cmp(void *priv, const void *a, const void *b) {

	(void) priv;

	return (fcore_when(a) < fcore_when(b));
}

static void
tus_exp_update(void *priv, void *p, unsigned u)
{
	struct tus_file_core *fcore;

	(void) priv;

	CAST_OBJ_NOTNULL(fcore, p, VMOD_TUS_FILE_CORE_MAGIC);

	fcore->exp_idx = u;
}

struct tus_exp *
tus_file_exp_new(void)
{
	struct tus_exp *e;
	pthread_attr_t attr;

	ALLOC_OBJ(e, TUS_EXP_MAGIC);
	AN(e);

	e->heap = VBH_new(NULL, tus_exp_cmp, tus_exp_update);
	AN(e->heap);

	AZ(pthread_mutex_init(&e->mtx, NULL));
	AZ(pthread_cond_init(&e->cond, NULL));

	AZ(pthread_attr_init(&attr));
	AZ(pthread_attr_setstacksize(&attr, (size_t)PTHREAD_STACK_MIN));
	AZ(pthread_create(&e->thread, &attr, tus_exp_thread, e));
	AZ(pthread_attr_destroy(&attr));
	return (e);
}

/* stop the expiry thread */
void
tus_file_exp_stop(struct tus_exp *e)
{
	CHECK_OBJ_NOTNULL(e, TUS_EXP_MAGIC);

	e->die = 1;
	AZ(pthread_cond_signal(&e->cond));
	AZ(pthread_join(e->thread, NULL));
	e->thread = 0;
}

void
tus_file_exp_destroy(struct tus_exp **ep)
{
	struct tus_exp *e;

	TAKE_OBJ_NOTNULL(e, ep, TUS_EXP_MAGIC);
	assert(e->die == 1);
	assert(e->thread == 0);
	AN(e->heap);
	AZ(VBH_root(e->heap));

	AZ(pthread_cond_destroy(&e->cond));
	AZ(pthread_mutex_destroy(&e->mtx));

	free(e);
}
