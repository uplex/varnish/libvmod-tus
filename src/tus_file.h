/*-
 * Copyright 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include <vtree.h>
#include <vtim.h>

#include "tus_server.h"

/* ============================================================
 * vmod shared object globals in vmod_tus.c
 */
extern struct VSC_lck *lck_fcore;

/* ============================================================
 * Util - could move elsewhere
 */

#define STRNCMP(var, literal)			\
	strncmp(var, literal, strlen(literal))
/*
 * lint:
 * 820: Boolean test of a parenthesized assignment
 * 774: Boolean within 'right side of && within if' always
 *      evaluates to True
 */
#define TOK(var, literal)						\
	(STRNCMP(var, literal) == 0 && ((var) += strlen(literal))	\
	 /*lint -e(820,774)*/)

/* ============================================================
 * tus_file
 */

#define TUS_PATH_MAX		((size_t)PATH_MAX)
#define TUS_METADATA_MAX	((size_t)2048)	// ballpark of AWS S3 metadata

enum tus_f_type {
	TUS_SINGLE = 0,
	TUS_PARTIAL,
	TUS_FINAL,
	_TUS_TYPE_LIMIT
};

// header of disk file - mmaped
struct tus_file_disk {
	unsigned			magic;
#define VMOD_TUS_FILE_DISK_MAGIC	0x105f11ed
	unsigned			version;
	char				upload_path[TUS_PATH_MAX];
	unsigned			guard_magic;
	unsigned			upload_path_length;
	char				metadata[TUS_METADATA_MAX];
	unsigned			guard2_magic;
	unsigned			metadata_length;
	char				location[TUS_PATH_MAX];
	unsigned			guard3_magic;
	unsigned			location_length;
	ssize_t			upload_length;	// -1 == defer
	ssize_t			upload_offset;
	VCL_TIME			upload_expires;
	enum tus_f_type		type;
};

/* what ptr points to */
enum tus_file_core_ptr {
	TFCP_INVALID = 0,
	TFCP_CONCAT,
#define TFCP_READABLE(tfcp) ((tfcp) >= TFCP_EMPTY)
	TFCP_EMPTY,
	TFCP_MMAP_RO,
	TFCP_MMAP_RW
};

struct tus_file_core {
	unsigned			magic;
#define VMOD_TUS_FILE_CORE_MAGIC	0x105f11e0
	int				fd;
	char				*filename;	// at basedir
	struct VPFX(tus_server)	*server;

	VSPLAY_ENTRY(tus_file_core)	entry;

	struct lock			mtx;
	pthread_cond_t			cond;
	struct tus_file_disk		*disk;
	unsigned			exp_idx;

	/* srvrefs can be gained under the server mutex
	 * during lookup
	 *
	 * one srvref is owned by exp as long as the file
	 * is on the index (can be found)
	 *
	 * once the file gets deleted / expired, srvref
	 * gets transferred to ref.
	 */
	unsigned			srvref;
	/* additional references, e.g. by concats
	 * because concats can also take srvrefs, but
	 * return refs, ref can become negative.
	 */
	int				ref;

	enum tus_file_core_ptr		ptr_type;
	/* final mmap or concats list */
	void				*ptr;
	size_t				len;
};

static inline const char *
tus_file_disk_err(const struct tus_file_disk *d)
{
	if (d->magic != VMOD_TUS_FILE_DISK_MAGIC)
		return ("bad magic");
	if (d->guard_magic != VMOD_TUS_FILE_DISK_MAGIC)
		return ("bad guard_magic");
	if (d->guard2_magic != VMOD_TUS_FILE_DISK_MAGIC)
		return ("bad guard2_magic");
	if (d->guard2_magic != VMOD_TUS_FILE_DISK_MAGIC)
		return ("bad guard3_magic");
	if (d->version != 2)
		return ("version != 2");
	if (strnlen(d->upload_path, TUS_PATH_MAX) != d->upload_path_length)
		return ("upload_path_length mismatch");
	if (strnlen(d->location, TUS_PATH_MAX) != d->location_length)
		return ("location_length mismatch");
	if (strnlen(d->metadata, TUS_METADATA_MAX) != d->metadata_length)
		return ("metadata_length mismatch");
	if (d->type >= _TUS_TYPE_LIMIT)
		return ("bad type");
	return (NULL);
}

#define CHECK_TUS_FILE_DISK(x) do {			\
		const char *err;			\
		AN(x);					\
		err = tus_file_disk_err(x);		\
		if (err != NULL)			\
			WRONG(err);			\
	} while(0);

static inline int
tus_file_cmp(const struct tus_file_core *a,
    const struct tus_file_core *b) {
	const struct tus_file_disk *aa, *bb;
	CHECK_OBJ_NOTNULL(a, VMOD_TUS_FILE_CORE_MAGIC);
	CHECK_OBJ_NOTNULL(b, VMOD_TUS_FILE_CORE_MAGIC);
	aa = a->disk;
	bb = b->disk;
	CHECK_TUS_FILE_DISK(aa);
	CHECK_TUS_FILE_DISK(bb);
	return (strcmp(aa->upload_path, bb->upload_path));
}

VSPLAY_HEAD(tus_files, tus_file_core);
VSPLAY_PROTOTYPE(tus_files, tus_file_core, entry, tus_file_cmp)

// tus_file.c
void tus_file_init(void);
void tus_file_load(struct VPFX(tus_server) *);
void tus_file_shutdown(struct VPFX(tus_server) *srv);
int tus_file_trylock(struct tus_file_core **);
void tus_file_unlock(struct tus_file_core **);
void tus_file_del(struct tus_file_core **);
void tus_file_exp(struct tus_file_core **);
struct tus_file_core *tus_file_new(VRT_CTX, struct VPFX(tus_server) *,
    enum tus_f_type, const char *, const char *, const char *,
    unsigned *, const char **);
struct tus_file_core *tus_file_lookup(struct VPFX(tus_server) *, const char *);
unsigned tus_body_to_file(VRT_CTX, struct tus_file_core *);
void tus_file_complete(struct tus_file_core *);

const char *
tus_file_final_urls(VRT_CTX, const struct tus_file_core *, const char *);

struct concat_embryo {
	unsigned		magic;
#define CONCAT_EMBRYO_MAGIC	0x150c05e5
	// to be written to fcore->fd once we have it
	struct VPFX(tus_server) *srv;
	struct vsb		*spec_vsb;
	struct tus_concat	*concat;
	size_t			concat_l;
	ssize_t		upload_length;
};

struct concat_embryo * tus_file_final_concat(struct VPFX(tus_server) *,
    struct concat_embryo *, const char *);
VCL_BLOB tus_concat_hash(VRT_CTX, const struct VPFX(tus_server) *,
    const struct tus_concat *);
void tus_file_final_birth(struct tus_file_core **, struct concat_embryo *);
void tus_file_final_abort(struct concat_embryo *);
VCL_BOOL tus_file_done(struct tus_file_core *, struct tus_file_disk *,
    const char *);
void tus_file_sync(struct tus_file_core *);
VCL_BOOL tus_file_meta(VRT_CTX, const struct tus_file_disk *, const char *,
    VCL_BLOB *);
